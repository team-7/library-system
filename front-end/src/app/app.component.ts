import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './core/services/auth.service';
import { ShowUserDTO } from './features/users/models/ShowUser.dto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'front-end';

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  public isLoggedIn: boolean;
  public user: ShowUserDTO;

  constructor(private readonly authService: AuthService) {

  }

  ngOnInit() {
    this.loggedInSubscription = this.authService.isLogged$.subscribe(
      (data) => {
        this.isLoggedIn = data;
      }
    );

    this.userSubscription = this.authService.loggedUser$.subscribe(
    (data) => {
      this.user = data;
    }
    );

  }

  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

}
