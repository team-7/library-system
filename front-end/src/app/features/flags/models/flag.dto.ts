export class FlagDTO {
    id: string;
    flagType: string;
    reviewId: string;
    isChecked: boolean;
}
