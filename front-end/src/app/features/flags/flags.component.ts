import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-flags',
  templateUrl: './flags.component.html',
  styleUrls: ['./flags.component.css']
})
export class FlagsComponent implements OnInit {

  @Input() flagsCount: number;
  @Output() newFlagEvent = new EventEmitter<{flagType: string}>();
  createFlag = false;
  constructor() { }

  ngOnInit() {}

  showCreateFlag() {
    this.createFlag = !this.createFlag;
  }

  flag(flag: {flagType: string}) {
    this.newFlagEvent.emit(flag);
    this.flagsCount++;
  }

}
