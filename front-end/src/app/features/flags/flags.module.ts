import { NgModule } from '@angular/core';
import { FlagsComponent } from './flags.component';
import { CreateFlagComponent } from './create-flag/create-flag.component';
import { SharedModule } from '../../shared/shared.module';



@NgModule({
  declarations: [
    FlagsComponent,
    CreateFlagComponent
  ],
  imports: [
    SharedModule
  ],
  exports: [
    FlagsComponent,
  ]
})
export class FlagsModule { }
