import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { CreateFlagDTO } from '../models/create-flag.dto';

@Component({
  selector: 'app-create-flag',
  templateUrl: './create-flag.component.html',
  styleUrls: ['./create-flag.component.css']
})
export class CreateFlagComponent implements OnInit {

  @Output() closedMenu = new EventEmitter<boolean>();
  @Output() newFlagEvent = new EventEmitter<CreateFlagDTO>();

  public selectedOption: string;
  public printedOption: string;

  public options = [
    { name: 'Spam', value: 'Spam' },
    { name: 'Unappropriate', value: 'Unappropriate' },
    { name: 'Offending', value: 'Offending' },
    { name: 'Other', value: 'Other' },
  ];

  constructor() { }

  ngOnInit() {

  }

  closeMenu() {
    this.closedMenu.emit(false);
  }

  flag() {
    this.newFlagEvent.emit({flagType: this.selectedOption});
    this.closeMenu();
  }

}
