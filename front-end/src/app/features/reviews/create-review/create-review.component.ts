import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-review',
  templateUrl: './create-review.component.html',
  styleUrls: ['./create-review.component.css']
})
export class CreateReviewComponent implements OnInit {

  public createReviewForm: FormGroup;

  @Output() newReviewEvent = new EventEmitter<{text: string}>();

  constructor(
    private readonly fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.createReviewForm = this.fb.group({
      text: [``, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(500)])],
    });
  }

  createReview() {
    this.newReviewEvent.emit(this.createReviewForm.value);
    this.createReviewForm.reset();
  }

}
