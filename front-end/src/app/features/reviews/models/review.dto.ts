export class ReviewDTO {
  public id: string;
  public text: string;
  public date: string;
  public isDeleted: boolean;
  public voteUpCount: number;
  public voteDownCount: number;
  public flagsCount: number;
  public usersWithUpVote: string[];
  public usersWithDownVote: string[];
  public usersWithFlag: string[];
  public user: {
    username: string,
    roles: string[]
  };
}
