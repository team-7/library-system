import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AllReviewsComponent } from './all-reviews/all-reviews.component';
import { CreateReviewComponent } from './create-review/create-review.component';
import { ReviewInfoComponent } from './review-info/review-info.component';
import { VotesModule } from '../votes/votes.module';
import { FlagsModule } from '../flags/flags.module';
import { UpdateReviewComponent } from './update-review/update-review.component';

@NgModule({
  imports: [SharedModule, FormsModule, VotesModule, FlagsModule],
  declarations: [
    AllReviewsComponent,
    CreateReviewComponent,
    ReviewInfoComponent,
    UpdateReviewComponent,
  ],
  providers: [],
  exports: [AllReviewsComponent]
})
export class ReviewsModule {}

