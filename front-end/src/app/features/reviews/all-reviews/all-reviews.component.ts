import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AuthService } from '../../../core/services/auth.service';
import { FlagsService } from '../../../core/services/flags.service';
import { NotificationService } from '../../../core/services/notification.service';
import { ReviewsService } from '../../../core/services/reviews.service';
import { VotesService } from '../../../core/services/votes.service';
import { ShowUserDTO } from '../../users/models/ShowUser.dto';
import { ReviewDTO } from '../models/review.dto';

@Component({
  selector: 'app-all-reviews',
  templateUrl: './all-reviews.component.html',
  styleUrls: ['./all-reviews.component.css']
})
export class AllReviewsComponent implements OnInit, OnDestroy {

  @Input() public bookId: string;
  public loggedUser: ShowUserDTO;
  public reviews: ReviewDTO[];


  constructor(
    private readonly reviewsService: ReviewsService,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly voteService: VotesService,
    private readonly flagService: FlagsService
  ) { }

  ngOnInit() {
    this.reviewsService.getBookReviews(this.bookId).subscribe(
      (res) => {
        this.reviews = res.map(o => {
          o.date = moment(o.date).fromNow();
          return o;
        });
      }
    );
    this.authService.loggedUser$.subscribe(
      (loggedUser) => {
        this.loggedUser = loggedUser;
      }
    );
  }

  ngOnDestroy(): void {
  }

  updateReview(updatedReview: {reviewId: string, newText: {text: string}}) {
    this.reviewsService.updateReview(this.bookId, updatedReview.reviewId, updatedReview.newText).subscribe(
      () => {
        this.notificationService.success(`The review was updated successfully!`);
      },
      () => {
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      }
    );
  }

  deleteReview(reviewId: string) {
    this.reviewsService.deleteReview(this.bookId, reviewId).subscribe(
      (res) => {
        if (!this.loggedUser.roles.includes('Admin')) {
          this.reviews = this.reviews.filter((r) => r.id !== reviewId);
        }
        this.notificationService.success(`${res.message}`);
      },
      () => {
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      }
    );
  }

  createReview(newReview: {text: string}) {
    this.reviewsService.createReview(this.bookId, newReview).subscribe(
      (res) => {
        res.date = moment(res.date).fromNow();
        this.reviews = [res, ...this.reviews];
        this.notificationService.success(`The review was created successfully!`);
      },
      () => {
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      }
    );
  }

  vote(vote: {voteType: string, reviewId: string, operation: string}) {
    if (vote.operation === 'create') {
      this.voteService.createVote(this.bookId, vote.reviewId, {voteType: vote.voteType}).subscribe(
        (_) => {},
        () => {
          this.notificationService.error(`Sorry a problem occurred, please try again later!`);
        }
      );
    } else if (vote.operation === 'update') {
      let voteId: string;
      this.voteService.getVoteIdByUser(this.bookId, vote.reviewId).subscribe(
        (res: {voteId: string}) => {
          voteId = res.voteId;
          this.voteService.updateVote(this.bookId, vote.reviewId, voteId, {voteType: vote.voteType}).subscribe(
            () => {},
            () => {
              this.notificationService.error(`Sorry a problem occurred, please try again later!`);
            }
          );
        },
        () => {
          this.notificationService.warn('No such vote to this user!');
        }
      );
    } else {
      let voteId = ' ';
      this.voteService.getVoteIdByUser(this.bookId, vote.reviewId).subscribe(
        (res: {voteId: string}) => {
          voteId = res.voteId;
          this.voteService.deleteVote(this.bookId, vote.reviewId, voteId).subscribe(
            (_) => {},
            () => {
              this.notificationService.error(`Sorry a problem occurred, please try again later!`);
            }
          );
        },
        () => {
          this.notificationService.warn('No such vote to this user!');
        }
      );
    }
  }

flag(flag: {flagType: string, reviewId: string}) {
    this.flagService.createFlag(this.bookId, flag.reviewId, {flagType: flag.flagType}).subscribe(
      () => {
        this.notificationService.success('Your flag was send successfully');
      },
      () => {
        this.notificationService.error('Sorry a problem occurred, please try again later!');
      }
    );
  }
}
