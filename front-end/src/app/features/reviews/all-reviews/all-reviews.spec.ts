import { ComponentFixture, TestBed, async, ɵMetadataOverrider } from '@angular/core/testing';
import { AllReviewsComponent } from './all-reviews.component';
import { of } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';
import { ReviewsService } from '../../../core/services/reviews.service';
import { VotesService } from '../../../core/services/votes.service';
import { FlagsService } from '../../../core/services/flags.service';
import { CreateReviewComponent } from '../create-review/create-review.component';
import { ReviewInfoComponent } from '../review-info/review-info.component';
import { Component } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { NotificationService } from '../../../core/services/notification.service';
import { FormsModule } from '@angular/forms';
import { VotesModule } from '../../votes/votes.module';
import { FlagsModule } from '../../flags/flags.module';
import { UpdateReviewComponent } from '../update-review/update-review.component';
import { ReviewDTO } from '../models/review.dto';

describe('Testin All Reviews Component', () => {

    let reviewsService;
    let authService;
    let voteService;
    let flagService;
    let notificationService;

    let fixture: ComponentFixture<AllReviewsComponent>;
    let component: AllReviewsComponent;

    beforeEach(async (() => {
        jest.clearAllMocks();

        reviewsService = {
            getBookReviews() {
                return of();
            },
            updateReview() {
                return of();
            },
            deleteReview() {
                return of();
            },
            createReview() {
               return of();
            }
        };

        authService = {
            get loggedUser$() {
                return of();
            }
        };
        flagService = {

        };
        voteService = {

        };
        notificationService = {
            success() {},
            warn() {},
            error() {}
        };

        @Component({
            selector: 'app-create-reviews',
            template: '<p>Mock Create Reviews Component</p>'
        })
        class MockCreateReviewsComponent {}

        @Component({
            selector: 'app-review-info',
            template: '<p>Mock Review Info Component</p>'
        })
        class MockReviewInfoComponent {}

        TestBed.configureTestingModule({
            imports: [SharedModule, FormsModule, VotesModule, FlagsModule],
            declarations: [AllReviewsComponent, UpdateReviewComponent, CreateReviewComponent, ReviewInfoComponent],
            providers: [AuthService, ReviewsService, VotesService, FlagsService, NotificationService]
        }).overrideProvider(AuthService, {useValue: authService})
            .overrideProvider(ReviewsService, {useValue: reviewsService})
            .overrideProvider(VotesService, {useValue: voteService})
            .overrideProvider(FlagsService, {useValue: flagService})
            .overrideProvider(NotificationService, {useValue: notificationService})
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(AllReviewsComponent);
                component = fixture.componentInstance;
        });

    }));

    it('should be defined', () => {
        expect(component).toBeDefined();
    });

    describe('ngOnInit() should', () => {
        it('subscribe to reviewsService.getBookReviews() once', () => {
            // Arrange
            const spy = jest.spyOn(reviewsService, 'getBookReviews').mockReturnValue(of('reviews'));
            const spy2 = jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of('user'));
            // Act
            component.ngOnInit();
            // Arrange
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('subscribe to authService.loggedUser$() once', () => {
            // Arrange
            const mockReview = new ReviewDTO();
            const mockReviews = [mockReview];
            const spy = jest.spyOn(reviewsService, 'getBookReviews').mockReturnValue(of(mockReviews));
            const spy2 = jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of('user'));
            // Act
            component.ngOnInit();
                  
            // Arrange
            expect(spy2).toHaveBeenCalledTimes(1);
        });
    });
});
