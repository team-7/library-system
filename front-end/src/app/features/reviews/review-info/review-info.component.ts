import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ReviewDTO } from '../models/review.dto';
import { AuthService } from '../../../core/services/auth.service';
import { ShowUserDTO } from '../../users/models/ShowUser.dto';
import { CreateFlagDTO } from '../../flags/models/create-flag.dto';

@Component({
  selector: 'app-review-info',
  templateUrl: './review-info.component.html',
  styleUrls: ['./review-info.component.css']
})
export class ReviewInfoComponent implements OnInit, OnDestroy {

  @Input() review: ReviewDTO;
  @Input() user: ShowUserDTO;
  @Output() newTextEvent = new EventEmitter<{reviewId: string, newText: {text: string}}>();
  @Output() deleteReviewEvent = new EventEmitter<string>();
  @Output() newVoteEvent = new EventEmitter<{voteType: string, reviewId: string, operation: string}>();
  @Output() newFlagEvent = new EventEmitter<{flagType: string, reviewId: string}>();
  public canEdit: boolean;
  public isEditMenuOpen = false;

  constructor() { }

  ngOnInit() {
    if ((this.user.username !== this.review.user.username) && (!this.user.roles.includes('Admin'))) {
      this.canEdit = false;
    } else {
      this.canEdit = true;
    }
  }

  ngOnDestroy(): void {
  }

  toggleEditMenu() {
    this.isEditMenuOpen = this.isEditMenuOpen ? false : true;
  }

  updateReview(newText: {text: string}) {
    const updatedReview = {
      reviewId: this.review.id,
      newText
    };
    this.review.text = newText.text;
    this.newTextEvent.emit(updatedReview);
  }

  deleteReview() {
    this.deleteReviewEvent.emit(this.review.id);
    this.review.isDeleted = true;
  }

  vote(vote: {voteType: string, operation: string}) {
    this.newVoteEvent.emit({...vote, reviewId: this.review.id});
    this.updateVoteCounts(vote);
  }

  flag(flag: CreateFlagDTO) {
    this.newFlagEvent.emit({...flag, reviewId: this.review.id});
  }

  private updateVoteCounts(vote: {voteType: string, operation: string}) {
    if (vote.operation === 'create') {
      if (vote.voteType === 'Up') {
        this.review.voteUpCount++;
      } else {
        this.review.voteDownCount++;
      }
    } else if (vote.operation === 'delete') {
      if (vote.voteType === 'Up') {
        this.review.voteUpCount--;
      } else {
        this.review.voteDownCount--;
      }
    } else if (vote.operation === 'update') {
      if (vote.voteType === 'Up') {
        this.review.voteUpCount++;
        this.review.voteDownCount = this.review.voteDownCount > 0 ? 0 : this.review.voteDownCount--;
      } else {
        this.review.voteDownCount++;
        this.review.voteUpCount = this.review.voteUpCount > 0 ? 0 : this.review.voteUpCount--;
      }
    }
  }

}
