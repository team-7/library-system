import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VoteDTO } from './models/vote.dto';
import { ShowUserDTO } from '../users/models/ShowUser.dto';
import { VotesService } from '../../core/services/votes.service';

@Component({
  selector: 'app-votes',
  templateUrl: './votes.component.html',
  styleUrls: ['./votes.component.css']
})
export class VotesComponent implements OnInit {

  @Input() user: ShowUserDTO;
  @Input() voteUpCount: number;
  @Input() voteDownCount: number;
  @Input() usersWithUpVote: string[];
  @Input() usersWithDownVote: string[];

  @Output() newVoteEvent = new EventEmitter<{voteType: string, operation: string}>();

  public curVote: string;

  constructor() { }

  ngOnInit() {
    if (this.usersWithUpVote.includes(this.user.username)) {
      this.curVote = 'Up';
    } else if (this.usersWithDownVote.includes(this.user.username)) {
      this.curVote = 'Down';
    } else {
      this.curVote = 'None';
    }
  }

  vote(vote: string) {
    let operation: string;
    if (this.curVote === 'None') {
      operation = 'create';
      this.curVote = vote;
    } else {
      if (vote === this.curVote) {
        operation = 'delete';
        this.curVote = 'None';
      } else if (vote !== this.curVote) {
        operation = 'update';
        this.curVote = vote;
      }
    }
    this.newVoteEvent.emit({voteType: vote, operation});
  }

}
