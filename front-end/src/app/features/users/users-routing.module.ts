import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { userRoutes } from './user.routes';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
  ],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
