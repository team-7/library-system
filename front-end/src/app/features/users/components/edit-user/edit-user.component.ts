import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { emailRegEx } from '../../../../common/constants/emailRegEx';
import { uniqueEmailValidator } from '../../../../common/validators/email.validator';
import { uniqueUsernameValidator } from '../../../../common/validators/username.validator';
import { DialogService } from '../../../../core/services/dialog.service';
import { UsersService } from '../../../../core/services/users.service';
import { DialogComponent } from '../../../../shared/components/dialog/dialog.component';
import { ShowUserDTO } from '../../models/ShowUser.dto';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  public user: ShowUserDTO;

  public editUserForm: FormGroup;

  constructor(
    private readonly usersService: UsersService,
    private readonly formBuilder: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly dialogService: DialogService,
  ) {
  }

  ngOnInit() {

    this.route.data.subscribe(
      (data) => {
        this.user = data.user;
      }
    );

    this.editUserForm = this.formBuilder.group({
      username: [this.user.username, [Validators.required, Validators.minLength(6)]],
      email: [this.user.email, [Validators.required, Validators.pattern(emailRegEx)]],
    },
    {
      validators: [
        uniqueEmailValidator('email', this.usersService, this.user.email),
        uniqueUsernameValidator('username', this.usersService, this.user.username),
      ],
      updateOn: 'blur',
    });

  }

  canDeactivate(): Observable<boolean> | boolean {
    return this.dialogService.open(DialogComponent);
  }

    editUser() {

    }

}
