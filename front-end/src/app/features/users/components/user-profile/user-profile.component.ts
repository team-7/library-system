import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../../core/services/auth.service';
import { StorageService } from '../../../../core/services/localStorage.service';
import { UsersService } from '../../../../core/services/users.service';
import { ShowUserDTO } from '../../models/ShowUser.dto';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {

  public user: ShowUserDTO;


  // NB! this should be removed and made by proper binding
  private loggedUserSubscription: Subscription;

  public loggedUser: ShowUserDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly usersService: UsersService,
    private readonly storageService: StorageService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (data) => this.user = data.user,
    );

    this.loggedUserSubscription = this.authService.loggedUser$.subscribe(
      (data) => {
        this.loggedUser = data;
      },
    );
  }

  public get mainRole() {
    return this.user.roles.includes('Admin') ? 'Admin' : 'User';
  }

  public changePriviledges() {

  }

  getEditUser() {
    this.router.navigateByUrl(`/users/${this.user.id}/edit-info`, { state: { username: this.user.username, email: this.user.email }});
  }

  deleteMyAcc() {
    this.usersService.deleteMyAcc(this.loggedUser.id).subscribe(
      (_) => {
        this.storageService.clear();
        this.router.navigate(['/landing']);
      },
      (error) => {
        // fail silently;

      }
    );
  }

  banUser() {
    this.usersService.banUser(this.user.id).subscribe(
      (data) => {
        this.user = data;
      },
    (error) => {/*fail silently*/},
    );
  }

  unbanUser() {
    this.usersService.unbanUser(this.user.id).subscribe(
      (data) => {
        this.user = data;
      }
    );
  }

  giveAdminPriviledges() {
    this.usersService.makeUserAnAdmin(this.user.id).subscribe(
      (data) => {
        this.user = data;
      }
    );
  }

  fireAdmin() {
    this.usersService.fireAdmin(this.user.id).subscribe(
      (data) => {
        this.user = data;
      }
    );
  }

  ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }

}
