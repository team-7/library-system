import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { passwordRegEx } from '../../../../common/constants/passwordRegEx';
import { matchingPasswords } from '../../../../common/validators/passwordMatch.validator';
import { AuthService } from '../../../../core/services/auth.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { DialogComponent } from '../../../../shared/components/dialog/dialog.component';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.css']
})
export class EditPasswordComponent {

  editPasswordForm: FormGroup;
  isUpdating = true;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly dialogService: DialogService,
  ) {
    this.editPasswordForm = formBuilder.group({
      currPassword: ['', [Validators.required, Validators.pattern(passwordRegEx)]],
      newPassword: ['', [Validators.required, Validators.pattern(passwordRegEx)]],
      confirmPassword: ['', [Validators.required, Validators.pattern(passwordRegEx)]],
    },
    {
      validators: matchingPasswords('newPassword', 'confirmPassword'),
      updateOn: 'blur',
    });

  }

  canDeactivate(): Observable<boolean> | boolean {
    return this.dialogService.open(DialogComponent);
  }

  changePass() {
    this.isUpdating = false;
    const passwords = { currPassword: this.editPasswordForm.value.currPassword, newPassword: this.editPasswordForm.value.newPassword };
    this.authService.changePassword(passwords).subscribe(
      () => {
        this.authService.logout();
      },
      (error) => {
        // fail silently;
      }
    );
  }

}
