import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search-users',
  templateUrl: './search-users.component.html',
  styleUrls: ['./search-users.component.css']
})
export class SearchUsersComponent {

  public keyword = '';

  @Output() search = new EventEmitter<string>();

  public triggerSearch(key: string) {
    this.search.emit(key);
  }

}
