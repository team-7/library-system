import { Component, Input, OnInit } from '@angular/core';
import { UsersService } from '../../../../core/services/users.service';
import { BookDTO } from '../../../books/models/bookDTO';
import { ShowUserDTO } from '../../models/ShowUser.dto';

@Component({
  selector: 'app-user-reading-now',
  templateUrl: './user-reading-now.component.html',
  styleUrls: ['./user-reading-now.component.css']
})
export class UserReadingNowComponent implements OnInit {

  @Input() public user: ShowUserDTO;
  public books: BookDTO[];
  constructor(
    private readonly usersService: UsersService,
  ) { }

  ngOnInit() {
    this.usersService.getUsersBooks(this.user.id).subscribe(
      (data) => {
        this.books = data;
      }
    );
  }

}
