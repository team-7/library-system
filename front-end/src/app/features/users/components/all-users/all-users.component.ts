import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ShowUserDTO } from '../../models/ShowUser.dto';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {
  displayedColumns: string[] = ['id', 'avatar', 'username', 'email'];
  dataSource: MatTableDataSource<ShowUserDTO>;

  public users: ShowUserDTO[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) {

    const observable = this.route.data.subscribe(
      (data) => {
        this.users = [ ...data.users ];

        const usersSource = [ ...data.users ];
        usersSource.map((val, index) => {
          // tslint:disable-next-line: no-string-literal
          val['tableId'] = index + 1;
        });
        this.dataSource = new MatTableDataSource(usersSource);

      }
    );

    observable.unsubscribe();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public triggerSelectUser(user: ShowUserDTO) {
    this.router.navigate([`/users/${user.id}`]);
  }

}
