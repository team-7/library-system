import { Levels } from '../../../common/enums/levels.enum';
import { UserRating } from '../../../common/enums/user-rating.enum';

export class ShowUserDTO {

  public avatar: string;

  public id: string;

  public username: string;

  public email: string;

  public gamingp: number;

  public level: Levels;

  public karmap: number;

  public rating: UserRating;

  public roles: string[];

  public banned: boolean;
}

