export class JwtPayloadDTO {
  username: string;
  id: string;
  roles: string[];
  banned: boolean;
}
