export class RegisterUserDto {
  avatar: string;

  email: string;

  username: string;

  password: string;
}
