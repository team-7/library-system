export class EmailVerificationDTO {
  public to: string;
  public from: string;
  public content: string;
  public subject: string;
  public textPart: string;
  public HTMLPart: string;
}
