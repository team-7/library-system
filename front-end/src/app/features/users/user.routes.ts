import { Routes } from '@angular/router';
import { AdminGuard } from '../../common/guards/admin-guard.service';
import { AuthGuard } from '../../common/guards/auth-guard.service';
import { CanDeactivateGuard } from '../../common/guards/deactivate-guard.service';
import { NotFoundComponent } from '../../shared/components/not-found/not-found.component';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { EditPasswordComponent } from './components/edit-password/edit-password.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AllUsersResolverService } from './resolvers/all-users-resolver.service';
import { UserResolverService } from './resolvers/user-resolver.service';

export const userRoutes: Routes = [
  {
    path: '',
    component: AllUsersComponent,
    resolve: { users: AllUsersResolverService },
    canActivate: [AdminGuard],
    data: { role: 'Admin' },
  },
  {
    path: ':id',
    component: UserProfileComponent,
    resolve: { user: UserResolverService },
    canActivate: [AuthGuard],
  },
  {
    path: ':id/edit-password',
    component: EditPasswordComponent,
    canActivate: [AuthGuard],
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: ':id/edit-info',
    component: EditUserComponent,
    resolve: { user: UserResolverService },
    canActivate: [AuthGuard],
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: '**',
    component: NotFoundComponent,
  }
];
