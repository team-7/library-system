import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { EditPasswordComponent } from './components/edit-password/edit-password.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { SearchUsersComponent } from './components/search-users/search-users.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserReadingNowComponent } from './components/user-reading-now/user-reading-now.component';
import { AllUsersResolverService } from './resolvers/all-users-resolver.service';
import { UserResolverService } from './resolvers/user-resolver.service';
import { UsersRoutingModule } from './users-routing.module';

@NgModule({
  declarations: [
    AllUsersComponent,
    UserProfileComponent,
    SearchUsersComponent,
    EditPasswordComponent,
    EditUserComponent,
    UserReadingNowComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsersRoutingModule,
  ],
  providers: [
    AllUsersResolverService,
    UserResolverService,
  ],
  exports: [
  ],

})
export class UsersModule {}
