import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { booksRouting } from './books.routing';


@NgModule({
  imports: [RouterModule.forChild(booksRouting)],
  exports: [RouterModule]
})
export class BooksRoutingModule {}
