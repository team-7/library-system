import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookDTO } from '../models/bookDTO';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-info',
  templateUrl: './book-info.component.html',
  styleUrls: ['./book-info.component.css']
})
export class BookInfoComponent implements OnInit {

  @Input() public book: BookDTO;

  @Output() public deleteBookEvent = new EventEmitter<string>();

  private readonly API_DOMAIN_NAME = 'http://localhost:3000/';
  private readonly BOOK_IMAGE_SRC_PREFIX = `${this.API_DOMAIN_NAME}bookCovers/`;
  private readonly DEFAULT_BOOK_IMAGE_PATH = '/assets/default-user.png';


  public bookImageSrcPrefix: string = this.BOOK_IMAGE_SRC_PREFIX;
  public defaultBookImagePath: string = this.DEFAULT_BOOK_IMAGE_PATH;

  constructor(private readonly router: Router) { }

  ngOnInit() {
  }

  public onClickInfo(book) {
    this.router.navigate([`books/`, book.id]);
  }

  public deleteBook() {
      this.deleteBookEvent.emit(this.book.id);
      this.book.isDeleted = true;
    }
}
