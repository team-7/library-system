import { Component, OnInit, Input, Directive, Output } from '@angular/core';
import { Router } from '@angular/router';
import { BooksService } from '../../core/services/books.service';
import { books } from './models/mockBooks';
import { BookDTO } from './models/bookDTO';
import { NotificationService } from '../../core/services/notification.service';
import { AuthService } from '../../core/services/auth.service';
import { ShowUserDTO } from '../users/models/ShowUser.dto';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  public allBooks: BookDTO[];
  public loggedUser: ShowUserDTO;


  constructor(
    private readonly booksService: BooksService,
    private readonly authService: AuthService
    ) {  }

  ngOnInit() {
    this.booksService.getAllBooks().subscribe(
      (booksFromDatabase: BookDTO[]) => {
        this.allBooks = booksFromDatabase;
      }
    );
    this.authService.loggedUser$.subscribe(
      (logged) => {
        this.loggedUser = logged;
      }
    );
  }
}
