export class CreateBookDTO {

  public name: string;

  public author: string;

  public text: string;

  public bookImgUrl: string;

}
