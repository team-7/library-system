import { BookDTO } from './bookDTO';

export const books: BookDTO[] = [
  {
    name: 'Shibana kniga',

    author: 'Shibaniq Joro',

    text: 'Shibana kniga ot Shibaniq Joro',

    borrowed: true,

    unlisted: false,

    bookRating: 5,

    bookImgUrl: './../../assets/Shibana kniga.jpg',

    id: 'f205f973-7a79-4e73-a5a6-0c01049673c1',

    isDeleted: false
  },
  {
    name: 'God',

    author: `God's messenger`,

    text: 'A book about god',

    borrowed: false,

    unlisted: false,

    bookRating: 3.5,

    bookImgUrl: './../../assets/God.jpg',

    id: 'f205f973-7a79-4e73-a5a6-0c01049673c1',

    isDeleted: false,
  },
  {
    name: 'How to remove the half of the world population',

    author: 'Thanos',

    text: 'if(population.rand()%2) extinguish',

    borrowed: true,

    unlisted: false,

    bookRating: 4,

    bookImgUrl: './../../assets/Thanos.jpg',

    id: 'f205f973-7a79-4e73-a5a6-0c01049673c1',

    isDeleted: false,
  },
  {
    name: 'Shibana kniga',

    author: 'Shibaniq Joro',

    text: 'Shibana kniga ot Shibaniq Joro',

    borrowed: true,

    unlisted: false,

    bookRating: 5,

    bookImgUrl: './../../assets/Shibana kniga.jpg',

    id: 'f205f973-7a79-4e73-a5a6-0c01049673c1',

    isDeleted: false,
  },
  {
    name: 'God',

    author: `God's messenger`,

    text: 'A book about god',

    borrowed: false,

    unlisted: false,

    bookRating: 3.5,

    bookImgUrl: './../../assets/God.jpg',

    id: 'f205f973-7a79-4e73-a5a6-0c01049673c1',

    isDeleted: false,
  },
  {
    name: 'How to remove the half of the world population',

    author: 'Thanos',

    text: 'if(population.rand()%2) extinguish',

    borrowed: true,

    unlisted: false,

    bookRating: 4,

    bookImgUrl: './../../assets/Thanos.jpg',

    id: 'f205f973-7a79-4e73-a5a6-0c01049673c1',

    isDeleted: false,
  },

];

