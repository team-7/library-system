export class BookDTO {

  public id: string;

  public name: string;

  public author: string;

  public text: string;

  public borrowed: boolean;

  public unlisted: boolean;

  public bookRating: number;

  public bookImgUrl: string;

  public isDeleted: boolean;
}
