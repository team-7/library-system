import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BooksService } from '../../../core/services/books.service';
import { NotificationService } from '../../../core/services/notification.service';
import { BookDTO } from '../models/bookDTO';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {

  private readonly API_DOMAIN_NAME = 'http://localhost:3000/api';

  public createForm: FormGroup;

  constructor(
    private readonly notification: NotificationService,
    private readonly bookService: BooksService,
    private readonly formBuilder: FormBuilder,
    private readonly http: HttpClient
    ) { }

  ngOnInit() {
    this.createForm = this.formBuilder.group(
      {
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      text: ['', [Validators.required, Validators.minLength(1)]],
    }
    );


  }

  public createBook(file: File, value) {

    this.bookService.createBook(value).subscribe(
      (book) => {
        const formData = new FormData();
        formData.append('file', file);

        this.http.put<BookDTO>(`${this.API_DOMAIN_NAME}/books/${book.id}/booksImg`, formData).subscribe((_) => {});
        this.notification.success('Book created');
      },
      (error) => this.notification.error(error),
    );
  }
}
