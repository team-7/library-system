import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BookDTO } from '../../models/bookDTO';
import { RateType } from './model/rateTypeDTO';

@Component({
  selector: 'ngbd-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class NgbdStarRatingComponent implements OnInit {

  @Input() book: BookDTO;
  public currentRate: number;

  @Output() rateEventEmitter: EventEmitter<RateType> = new EventEmitter();

  public constructor() {}
  ngOnInit() {
    this.currentRate = this.book.bookRating;

  }

  public emitRate(rate) {

   this.rateEventEmitter.emit(rate);
  }
}
