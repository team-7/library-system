import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NgbdStarRatingComponent } from './star-rating.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [NgbModule],
  declarations: [NgbdStarRatingComponent],
  exports: [NgbdStarRatingComponent],
  bootstrap: [NgbdStarRatingComponent]
})
export class NgbdStarRatingModule {}
