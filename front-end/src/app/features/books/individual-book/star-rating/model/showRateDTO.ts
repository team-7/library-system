import { RateType } from './rateTypeDTO';

export class ShowRateDTO {
  public rateType: RateType;
}
