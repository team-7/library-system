import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { BooksService } from '../../../core/services/books.service';
import { NotificationService } from '../../../core/services/notification.service';
import { RateService } from '../../../core/services/rate.service';
import { ShowUserDTO } from '../../users/models/ShowUser.dto';
import { BookDTO } from '../models/bookDTO';

@Component({
  selector: 'app-individual-book',
  templateUrl: './individual-book.component.html',
  styleUrls: ['./individual-book.component.css']
})
export class IndividualBookComponent implements OnInit {

  book: BookDTO;
  withReviews = false;
  public loggedUser: ShowUserDTO;


  private readonly API_DOMAIN_NAME = 'http://localhost:3000/';
  private readonly BOOK_IMAGE_SRC_PREFIX = `${this.API_DOMAIN_NAME}bookCovers/`;
  private readonly DEFAULT_BOOK_IMAGE_PATH = '/assets/default-user.png';

  public bookImageSrcPrefix: string = this.BOOK_IMAGE_SRC_PREFIX;
  public defaultBookImagePath: string = this.DEFAULT_BOOK_IMAGE_PATH;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly booksService: BooksService,
    private readonly authService: AuthService,
    private readonly notification: NotificationService,
    private readonly rateService: RateService,
    ) {
   }


  ngOnInit() {
    const bookId: string = this.activatedRoute.snapshot.paramMap.get('id');
    this.booksService.getBookById(bookId).subscribe(
      (foundBook: BookDTO) => {

        this.book = foundBook;
      },
    );
    this.authService.loggedUser$.subscribe(
      (logged) => {
        this.loggedUser = logged;
      }
    );
  }

  getBookReviews() {
    this.withReviews = !this.withReviews;
  }

  public deleteBook() {
    this.booksService.deleteBook(this.book.id).subscribe((message) => {
      this.book.isDeleted = true;
      this.notification.success(message.msg);
    });
  }

  public borrow() {
    const update = {
      borrowed: true,
      unlisted: this.book.unlisted,
    };
    this.booksService.updateBook(this.book.id, update).subscribe((body) => {
      this.book = body;
      this.notification.success('The book was borrowed!');
    },
    (error) => {
      this.notification.error(error.msg);
    }
    );
  }

  public return() {
    const update = {
      borrowed: false,
      unlisted: this.book.unlisted,
    };
    this.booksService.updateBook(this.book.id, update).subscribe((body) => {
      this.book = body;
      this.notification.success('The book was returned!');
    });
  }

  public unlist() {
    const update = {
      borrowed: this.book.borrowed,
      unlisted: true,
    };
    this.booksService.updateBook(this.book.id, update).subscribe((body) => {
      this.book = body;
      this.notification.success('The book was unlisted!');
    });
  }

  public list() {
    const update = {
      borrowed: this.book.borrowed,
      unlisted: false,
    };
    this.booksService.updateBook(this.book.id, update).subscribe((body) => {
      this.book = body;
      this.notification.success('The book was enlisted!');
    });
  }

  public rateBook(rate) {
    const R = {
      rateType: rate,
    };
    this.rateService.createRate(this.book.id, R).subscribe((rating) => {

      this.book.bookRating = rating;

      this.notification.success('Rated successfully');

    },
    (error) => {
      this.notification.error(error.msg);
    }
    );
  }
}
