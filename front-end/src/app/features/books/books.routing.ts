import { Routes } from '@angular/router';
import { BooksComponent } from './books.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { IndividualBookComponent } from './individual-book/individual-book.component';



export const booksRouting: Routes = [
  { path: 'create', component: CreateBookComponent },
  { path: '', component: BooksComponent },
  {
    path: ':id',
    component: IndividualBookComponent,
    loadChildren: () => import(`./../reviews/reviews.module`).then(m => m.ReviewsModule),
  },
];
