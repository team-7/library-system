import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../shared/shared.module';
import { ReviewsModule } from '../reviews/reviews.module';
import { BookInfoComponent } from './book-info/book-info.component';
import { BooksRoutingModule } from './books-routhing.module';
import { BooksComponent } from './books.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { IndividualBookComponent } from './individual-book/individual-book.component';
import { NgbdStarRatingModule } from './individual-book/star-rating/star-rating.module';
@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    BooksRoutingModule,
    ReviewsModule,
    NgbModule,
    NgbdStarRatingModule
   ],
    declarations: [
    BooksComponent,
    BookInfoComponent,
    CreateBookComponent,
    IndividualBookComponent,
  ],
  providers: []
})
export class BooksModule {}
