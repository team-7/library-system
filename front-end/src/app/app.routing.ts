import { Routes } from '@angular/router';
import { AuthGuard } from './common/guards/auth-guard.service';
import { NotLoggedGuard } from './common/guards/not-logged.service';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

export const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'landing',
    component: LandingPageComponent,
    canActivate: [NotLoggedGuard],
  },
  {
    path: 'home',
    component: HomePageComponent,
    pathMatch: 'full',
    loadChildren: () => import('./features/books/books.module').then(m => m.BooksModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'users',
    loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'books',
    loadChildren: () => import(`./features/books/books.module`).then(m => m.BooksModule),
  },
  {
    path: '**',
    component: NotFoundComponent,
  }
];
