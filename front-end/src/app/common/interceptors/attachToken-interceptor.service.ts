import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { StorageService } from '../../core/services/localStorage.service';

@Injectable()
export class AttachTokenInterceptor implements HttpInterceptor {

  constructor(
    private readonly storageService: StorageService,
    private readonly authService: AuthService,
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const token = this.storageService.getItem('token');

    if (token) {
      req = req.clone({
        setHeaders: {
          authorization: `Bearer ${token}`,
        }
      });
    }

    return next.handle(req);
  }
}
