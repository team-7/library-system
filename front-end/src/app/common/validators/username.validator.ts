import { FormGroup } from '@angular/forms';
import { UsersService } from '../../core/services/users.service';

export function uniqueUsernameValidator(prop: string, usersService: UsersService, initUsername?: string) {
  return (group: FormGroup) => {
    // control);
    const usernameControl = group.controls[prop];

    if (usernameControl.errors) {
      return;
    }

    if (initUsername && initUsername === usernameControl.value) {
      return;
    }

    usersService.getUserByUsername(usernameControl.value).subscribe(
      (data) => {
        if (data && data.length !== 0) {
          usernameControl.setErrors({ uniqueUsername: true });
        }
      }
    );

  };
}
