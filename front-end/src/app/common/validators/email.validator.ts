import { FormGroup } from '@angular/forms';
import { UsersService } from '../../core/services/users.service';

export function uniqueEmailValidator(prop: string, usersService: UsersService, initEmail?: string) {
  return (group: FormGroup) => {
    // control);
    const emailControl = group.controls[prop];

    if (emailControl.errors) {
      return;
    }

    if (initEmail && initEmail === emailControl.value) {
      return;
    }

    usersService.getUserByEmail(emailControl.value).subscribe(
      (data) => {
        if (data && data.length !== 0) {
          emailControl.setErrors({ uniqueEmail: true });
        }
      }
    );

  };
}
