import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../core/services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {


  constructor(
    private readonly authService: AuthService,
    private router: Router,
    ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.authService.isLogged$
        .pipe(
          tap(loggedIn => {

            if (!loggedIn) {
              this.router.navigate(['/landing']);
            }
          }),
        );

  }

}
