import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { StorageService } from '../../core/services/localStorage.service';

@Injectable()
export class NotLoggedGuard implements CanActivate {


  constructor(
    private readonly authService: AuthService,
    private router: Router,
    private readonly storageService: StorageService,
    ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const token: string = this.storageService.getItem('token');

    if (token) {
      this.router.navigate(['/home']);

      return false;
    }


    return true;


  }

}
