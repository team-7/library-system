import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { StorageService } from '../../core/services/localStorage.service';
import { JwtPayloadDTO } from '../../features/users/models/JwtPayload.dto';

@Injectable()
export class AdminGuard {

  constructor(
    private authService: AuthService,
    private router: Router,
    private StorageService: StorageService,
    ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const token: string = this.StorageService.getItem('token');

    if (!token) {
      this.router.navigate(['/landing']);
      return false;
    }
    const user: JwtPayloadDTO = this.authService.getDecodedAccessToken(token);

    if (!user.roles || !user.roles.includes(next.data.role)) {

      this.router.navigate(['/not-found']);
      return false;
    }

    return true;
  }

}
