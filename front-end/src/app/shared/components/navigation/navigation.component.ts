import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';
import { ShowUserDTO } from '../../../features/users/models/ShowUser.dto';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy {

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
  ) { }

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  public isLoggedIn: boolean;
  public user: ShowUserDTO;

  ngOnInit() {
    this.loggedInSubscription = this.authService.isLogged$.subscribe(
      (data) => {
        this.isLoggedIn = data;
      }
    );

    this.userSubscription = this.authService.loggedUser$.subscribe(
    (data) => {
      this.user = data;
    }
    );
  }

  public allBooks() {
    this.router.navigate(['/books']);
  }

  public allUsers() {
    this.router.navigate(['/users']);
  }

  public logout() {
    this.authService.logout();
  }

  public myProfile() {

    this.authService.loggedUser$.subscribe(
      (user: ShowUserDTO) => {
        if (user) {
          this.router.navigate(['/users', user.id]);
        }
      },
      (error) => {
        // fail silently;
        this.router.navigate(['/landing']);
      }
    );
  }

  public createBook() {
    this.router.navigate(['/books/create']);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.loggedInSubscription.unsubscribe();
  }

}
