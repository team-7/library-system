import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BookDTO } from '../../features/books/models/bookDTO';

import { CreateBookDTO } from '../../features/books/models/createBookDTO';

import { UpdateBookDTO } from '../../features/books/models/updateBookDTO';
import { ShowRateDTO } from '../../features/books/individual-book/star-rating/model/showRateDTO';
import { RateType } from '../../features/books/individual-book/star-rating/model/rateTypeDTO';
import { HttpClient } from '@angular/common/http';
import { ShowUserDTO } from '../../features/users/models/ShowUser.dto';

@Injectable({
  providedIn: 'root'
})
export class RateService {

  private readonly baseURL = 'http://localhost:3000/api/books';

  constructor( private readonly http: HttpClient) {}

  // public getRating(bookId: string): Observable<ShowRateDTO[]> {
  //   return this.http.get<ShowRateDTO[]>(`${this.baseURL}/${bookId}/rating`);
  // }

  public createRate(bookId: string, rate: ShowRateDTO): Observable<number> {
    return this.http.post<number>(`${this.baseURL}/${bookId}/rating`, rate);
  }
}
