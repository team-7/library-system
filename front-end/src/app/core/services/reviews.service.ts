import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShowReviewDTO } from '../../features/reviews/models/show-review.dto';
import { Observable } from 'rxjs';
import { ReviewDTO } from '../../features/reviews/models/review.dto';

@Injectable()
export class ReviewsService {

    private readonly baseURL = 'http://localhost:3000/api';

    constructor(
        private readonly http: HttpClient
    ) {}

    public getBookReviews(bookId: string): Observable<ReviewDTO[]> {
        return this.http.get<ReviewDTO[]>(`${this.baseURL}/books/${bookId}/reviews`);
    }

    public updateReview(bookId: string, reviewId: string, newText: {text: string}): Observable<ReviewDTO> {
        return this.http.put<ReviewDTO>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}`, newText);
    }

    public deleteReview(bookId: string, reviewId: string): Observable<{message: string}> {
        return this.http.delete<{message: string}>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}`);
    }

    public createReview(bookId: string, newReview: {text: string}): Observable<ReviewDTO> {
        return this.http.post<ReviewDTO>(`${this.baseURL}/books/${bookId}/reviews`, newReview);
    }
}
