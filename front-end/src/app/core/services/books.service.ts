import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BookDTO } from '../../features/books/models/bookDTO';
import { HttpClient } from '@angular/common/http';
import { CreateBookDTO } from '../../features/books/models/createBookDTO';
import { UpdateBookDTO } from '../../features/books/models/updateBookDTO';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private readonly baseURL = 'http://localhost:3000/api';

  constructor( private readonly http: HttpClient) {}

  public getAllBooks(): Observable<BookDTO[]> {
    return this.http.get<BookDTO[]>(`${this.baseURL}/books`);
  }

  public createBook(book: CreateBookDTO): Observable<BookDTO> {
    return this.http.post<BookDTO>(`${this.baseURL}/books`, book);
  }

  public getBookById(id: string): Observable<BookDTO> {
    return this.http.get<BookDTO>(`${this.baseURL}/books/${id}`);
  }

  public deleteBook(id: string): Observable<{msg: string}> {
    return this.http.delete<{msg: string}>(`${this.baseURL}/books/${id}`);
  }

  public updateBook(id: string, updateBook: UpdateBookDTO): Observable<BookDTO> {
    return this.http.put<BookDTO>(`${this.baseURL}/books/${id}/state`, updateBook);
  }
}
