import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VoteDTO } from '../../features/votes/models/vote.dto';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VotesService {

  private readonly baseURL = 'http://localhost:3000/api';

  constructor(
    private readonly http: HttpClient,
    ) {}

  public createVote(bookId: string, reviewId: string, vote: {voteType: string}): Observable<VoteDTO> {
    return this.http.post<VoteDTO>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}/votes`, vote);
  }

  public updateVote(bookId: string, reviewId: string, voteId: string, vote: {voteType: string}): Observable<VoteDTO> {
    return this.http.put<VoteDTO>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}/votes/${voteId}`, vote);
  }

  public deleteVote(bookId: string, reviewId: string, voteId: string): Observable<{message: string}> {
    return this.http.delete<{message: string}>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}/votes/${voteId}`);
  }

  public getVoteIdByUser(bookId: string, reviewId: string): Observable<{voteId: string}> {
    return this.http.get<{voteId: string}>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}/votes/user`);
  }
}
