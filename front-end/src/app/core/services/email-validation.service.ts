import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmailValidationService {

  constructor(
    private readonly http: HttpClient,
  ) {}

  sendEmail(emailConfig: any): Observable<any> {
    return this.http.post('http://localhost:3001/send-verification', emailConfig);
  }
}
