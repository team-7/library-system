import { TestBed } from '@angular/core/testing';
import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from 'selenium-webdriver/http';
import { HomePageComponent } from '../../components/home-page/home-page.component';
import { AuthService } from './auth.service';
import { StorageService } from './localStorage.service';



describe('AuthService', () => {

  const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomePageComponent },
  ];

  const http = {
    post() {},
  };
  const storage = {
    read() { return ''; },
    save() { },
  };

  const helper = {
    decode() { },
  };

  let getService: () => AuthService;
  let router: Router;

  beforeEach(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [HomePageComponent],
      providers: [
        AuthService,
        HttpClient,
        StorageService,
        JwtHelperService,
      ]
    })
    .overrideProvider(HttpClient, { useValue: http })
    .overrideProvider(StorageService, { useValue: storage })
    .overrideProvider(JwtHelperService, { useValue: helper });

    getService = () => TestBed.get(AuthService);
    router = TestBed.get(Router);
  });

});
