import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BookDTO } from '../../features/books/models/bookDTO';
import { LoginDTO } from '../../features/users/models/LoginUser.dto';
import { RegisterUserDto } from '../../features/users/models/RegisterUser.dto';
import { ShowUserDTO } from '../../features/users/models/ShowUser.dto';
import { TokenDTO } from '../../features/users/models/Token.dto';

@Injectable()
export class UsersService {

  private static readonly baseURL = 'http://localhost:3000/api';

  constructor(
    private readonly http: HttpClient,
  ) {}

  public logIn(user: LoginDTO): Observable<TokenDTO> {
    return this.http.post<TokenDTO>(`${UsersService.baseURL}/login`, user);
  }

  public signUp(user: RegisterUserDto): Observable<ShowUserDTO> {
    return this.http.post<ShowUserDTO>(`${UsersService.baseURL}/users`, user);
  }

  public allUsers(): Observable<any[]> {
    return this.http.get<ShowUserDTO[]>(`${UsersService.baseURL}/users`);
  }

  public getUserById(id: string): Observable<ShowUserDTO> {
    return this.http.get<ShowUserDTO>(`${UsersService.baseURL}/users/${id}`);
  }

  public getUserByEmail(email: string): Observable<ShowUserDTO[]> {
    return this.http.get<ShowUserDTO[]>(`${UsersService.baseURL}/users/query?email=${email}&username=null`);
  }

  public getUserByUsername(username: string): Observable<ShowUserDTO[]> {
    return this.http.get<ShowUserDTO[]>(`${UsersService.baseURL}/users/query?username=${username}&email=null`);
  }

  public getUsersBooks(id: string): Observable<BookDTO[]> {
    return this.http.get<BookDTO[]>(`${UsersService.baseURL}/users/${id}/borrowed`);
  }

  public deleteMyAcc(id: string): Observable<ShowUserDTO> {
    return this.http.delete<ShowUserDTO>(`${UsersService.baseURL}/users/${id}`);
  }

  public banUser(id: string) {
    return this.http.get<ShowUserDTO>(`${UsersService.baseURL}/users/${id}/ban`, {});
  }

  public unbanUser(id: string) {
    return this.http.get<ShowUserDTO>(`${UsersService.baseURL}/users/${id}/unban`);
  }

  public makeUserAnAdmin(id: string) {
    return this.http.put<ShowUserDTO>(`${UsersService.baseURL}/users/${id}/privileges`, ['User', 'Admin']);
  }

  public fireAdmin(id: string) {
    return this.http.put<ShowUserDTO>(`${UsersService.baseURL}/users/${id}/privileges`, ['User']);
  }
}
