import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ChangePasswordDTO } from '../../features/users/models/ChangePassword.dto';
import { LoginDTO } from '../../features/users/models/LoginUser.dto';
import { RegisterUserDto } from '../../features/users/models/RegisterUser.dto';
import { ShowUserDTO } from '../../features/users/models/ShowUser.dto';
import { StorageService } from './localStorage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly isLoggedSubject$: BehaviorSubject<boolean> = new BehaviorSubject(this.isUserLoggedIn());
  private readonly loggedUserSubject$: BehaviorSubject<ShowUserDTO> = new BehaviorSubject(this.loggedUser());

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly storageService: StorageService,
    private readonly helper: JwtHelperService,
  ) { }

  public logIn(user: LoginDTO) {
    const logInUser: LoginDTO = { username: user.username, password: user.password };
    return this.http.post<{ token: string }>(`http://localhost:3000/api/login`, logInUser)
      .pipe(
        tap(
          (data) => {
            try {
              const loggedUser = this.helper.decodeToken(data.token);

              const storageType: string = user.keepMeLoggedIn ? 'local' : 'session';

              this.storageService.setItem('token', data.token, storageType);

              this.isLoggedSubject$.next(true);
              this.loggedUserSubject$.next(loggedUser);

            } catch (error) {
              // proceed
            }
          },
        )
      );
  }

  public logout() {
    return this.http.get('http://localhost:3000/api/logout').subscribe(
      (_) => {
        this.storageService.clear();

        this.isLoggedSubject$.next(false);
        this.loggedUserSubject$.next(null);

        this.router.navigate(['/landing']);
      },
      (error) => {
        // fail silently;
        this.router.navigate(['/']);

      }
    );
  }

  public register(user: RegisterUserDto) {
    return this.http.post(`http://localhost:3000/api/users`, user);
  }

  public changePassword(passwords: ChangePasswordDTO) {
    return this.http.patch('http://localhost:3000/api/users/password', passwords);
  }


  private isUserLoggedIn() {
    const token = this.storageService.getItem('token');
    return !!token;
  }

  public get isLogged$(): Observable<boolean> {
    return this.isLoggedSubject$.asObservable();
  }

  public get loggedUser$(): Observable<ShowUserDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  public getDecodedAccessToken(token: string) {
    try {
      return this.helper.decodeToken(token);
    } catch (error) {
      // silent error
    }
  }

  private loggedUser(): ShowUserDTO {
    try {
      return this.helper.decodeToken(this.storageService.getItem('token'));
    } catch (error) {
      // in case of StorageService tampering
      this.isLoggedSubject$.next(false);

      return null;
    }
  }

}
