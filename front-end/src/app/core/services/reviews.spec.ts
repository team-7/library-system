import { AuthService } from './auth.service';
import { TestBed, async } from '@angular/core/testing';
import { of } from 'rxjs';
import { ReviewsService } from './reviews.service';
import { HttpClient } from '@angular/common/http';

describe('Reviews Service', () => {

    const http = {
        get() {
            return of();
        },
        post() {
            return of();
        },
        put() {
            return of();
        },
        delete() {
            return of();
        }
    };

    let getService: () => ReviewsService;

    beforeEach(async (() => {

        jest.clearAllMocks();

        TestBed.configureTestingModule({
            imports: [],
            declarations: [],
            providers: [
                HttpClient,
                ReviewsService
            ]
        }).overrideProvider(HttpClient, { useValue: http});

        getService = () => TestBed.get(ReviewsService);

    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(getService).toBeDefined();
    });

    describe('getBookReviews() should', () => {
        it('call http.get() once', () => {
            // Arrange
            const reviews = [
                {
                    id: '1',
                    text: 'a'
                },
                {
                   id: '2',
                   text: 'b'
               },
            ];
            const spy = jest.spyOn(http, 'get').mockReturnValue(of(reviews));
            const bookId = '1';
            // Act
            getService().getBookReviews(bookId);
            // Assert
            expect(spy).toHaveBeenCalled();
            expect(spy).toHaveBeenCalledTimes(1);

        });

        describe('updateReview() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const review = {
                    id: '1',
                    text: 'a'
                };

                const body = {
                    text: 'b'
                };

                const reviewId = '1';

                const spy = jest.spyOn(http, 'put').mockReturnValue(of(review));
                const bookId = '1';
                // Act
                getService().updateReview(bookId, reviewId, body);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });

        describe('deleteReview() should', () => {
            it('call http.delete() once', () => {
                // Arrange
                const message = 'message';

                const spy = jest.spyOn(http, 'delete').mockReturnValue(of(message));
                const bookId = '1';
                const reviewId = '1';
                // Act
                getService().deleteReview(bookId, reviewId);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });

        describe('createReview() should', () => {
            it('call http.post() once', () => {
                // Arrange
                const body = {
                    text: 'a'
                };
                const review = {
                    id: '1',
                    text: 'a'
                };
                const spy = jest.spyOn(http, 'post').mockReturnValue(of(review));
                const bookId = '1';
                // Act
                getService().createReview(bookId, body);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });
    });
});
