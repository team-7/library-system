import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FlagDTO } from '../../features/flags/models/flag.dto';

@Injectable({
  providedIn: 'root'
})
export class FlagsService {

  private readonly baseURL = 'http://localhost:3000/api';

  constructor(
    private readonly http: HttpClient
  ) {}

  public getAllFlags(bookId: string, reviewId: string): Observable<FlagDTO[]> {
    return this.http.get<FlagDTO[]>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}/flags`);
  }

  public createFlag(bookId: string, reviewId: string, flag: {flagType: string}): Observable<FlagDTO> {
    return this.http.post<FlagDTO>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}/flags`, flag);
  }

  public checkFlag(bookId: string, reviewId: string, flagId: string): Observable<{message: string}> {
    return this.http.delete<{message: string}>(`${this.baseURL}/books/${bookId}/reviews/${reviewId}/flags/${flagId}`);
  }
}
