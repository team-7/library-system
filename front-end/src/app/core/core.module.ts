import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { AdminGuard } from '../common/guards/admin-guard.service';
import { AuthGuard } from '../common/guards/auth-guard.service';
import { CanDeactivateGuard } from '../common/guards/deactivate-guard.service';
import { NotLoggedGuard } from '../common/guards/not-logged.service';
import { AuthService } from './services/auth.service';
import { DialogService } from './services/dialog.service';
import { FlagsService } from './services/flags.service';
import { StorageService } from './services/localStorage.service';
import { NotificationService } from './services/notification.service';
import { ReviewsService } from './services/reviews.service';
import { UsersService } from './services/users.service';
import { VotesService } from './services/votes.service';



@NgModule({
  declarations: [],
  imports: [
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
  ],
  providers: [
    UsersService,
    NotificationService,
    AuthService,
    AuthGuard,
    NotLoggedGuard,
    AdminGuard,
    CanDeactivateGuard,
    ReviewsService,
    FlagsService,
    VotesService,
    StorageService,
    DialogService,
  ],
  exports: [
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parent: CoreModule,
  ) {
    if (parent) {
      throw new Error('Core module is already initialized!');
    }
  }
}
