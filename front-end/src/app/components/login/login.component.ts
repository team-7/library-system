import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { passwordRegEx } from '../../common/constants/passwordRegEx';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

public loginForm: FormGroup;

constructor(
  public readonly formBuilder: FormBuilder,
  private readonly authService: AuthService,
  private readonly notificationService: NotificationService,
  private readonly router: Router,
) {
  this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      password: ['', [Validators.required, Validators.pattern(passwordRegEx)]],
      keepMeLoggedIn: [false],
  });
}

ngOnInit() {
}


public login() {

  this.authService.logIn(this.loginForm.value)
  .subscribe(
    () => {
      this.notificationService.success(`Login successful!`);
      this.router.navigate(['/home']);
    },
    () => this.notificationService.error(`Invalid email/password!`),
  );
}

}
