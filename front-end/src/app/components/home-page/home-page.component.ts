import { Component, OnInit } from '@angular/core';
import { books } from '../../features/books/models/mockBooks';
import { BookDTO } from '../../features/books/models/bookDTO';
import { BooksService } from '../../core/services/books.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public books = [];
  
  constructor(
    private readonly booksService: BooksService,
    ) {  }

  ngOnInit() {
    this.booksService.getAllBooks().subscribe(
      (booksFromDatabase: BookDTO[]) => {
        booksFromDatabase.sort((a: BookDTO, b: BookDTO) => b.bookRating - a.bookRating);

        this.books.push(booksFromDatabase[0]);
        this.books.push(booksFromDatabase[1]);
        this.books.push(booksFromDatabase[2]);
        this.books[0].bookRating = 5;
        this.books[1].bookRating = 4.9;
        this.books[2].bookRating = 4.7;
    });

  }

}
