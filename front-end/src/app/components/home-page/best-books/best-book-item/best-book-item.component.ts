import { Component, Input, OnInit } from '@angular/core';
import { BookDTO } from '../../../../features/books/models/bookDTO';

@Component({
  selector: 'app-best-book-item',
  templateUrl: './best-book-item.component.html',
  styleUrls: ['./best-book-item.component.css']
})
export class BestBookItemComponent implements OnInit {

  @Input() book: BookDTO;

  private readonly API_DOMAIN_NAME = 'http://localhost:3000/';
  private readonly BOOK_IMAGE_SRC_PREFIX = `${this.API_DOMAIN_NAME}bookCovers/`;
  private readonly DEFAULT_BOOK_IMAGE_PATH = '/assets/default-user.png';


  public bookImageSrcPrefix: string = this.BOOK_IMAGE_SRC_PREFIX;
  public defaultBookImagePath: string = this.DEFAULT_BOOK_IMAGE_PATH;

  constructor() { }

  ngOnInit() {
  }

  getUrl() {
    return `http://localhost:3000/bookCovers/${this.book.bookImgUrl}`;
  }

}
