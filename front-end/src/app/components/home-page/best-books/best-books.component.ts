import { Component, OnInit, Input } from '@angular/core';
import { BookDTO } from '../../../features/books/models/bookDTO';

@Component({
  selector: 'app-best-books',
  templateUrl: './best-books.component.html',
  styleUrls: ['./best-books.component.css']
})
export class BestBooksComponent implements OnInit {

  @Input() books: BookDTO[];

  constructor() { }

  ngOnInit() {
  }

}
