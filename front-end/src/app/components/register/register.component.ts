import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { passwordRegEx } from '../../common/constants/passwordRegEx';
import { uniqueEmailValidator } from '../../common/validators/email.validator';
import { matchingPasswords } from '../../common/validators/passwordMatch.validator';
import { uniqueUsernameValidator } from '../../common/validators/username.validator';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { UsersService } from '../../core/services/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

public registerForm: FormGroup;

public avatarUrls = [
  'assets/profile-avatars/boy.png',
  'assets/profile-avatars/child.png',
  'assets/profile-avatars/girl.png',
  'assets/profile-avatars/man.png',
  'assets/profile-avatars/woman.png',
];
public selectedAvatar = 'assets/profile-avatars/boy.png';
constructor(
  public readonly formBuilder: FormBuilder,
  private readonly authService: AuthService,
  private readonly notificationService: NotificationService,
  private readonly router: Router,
  private readonly usersService: UsersService,
) {
  this.registerForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.pattern(passwordRegEx)])],
      confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.pattern(passwordRegEx)])],
      keepMeLoggedIn: [false],
  },
  {
    validators: [
      uniqueEmailValidator('email', this.usersService),
      uniqueUsernameValidator('username', this.usersService),
      matchingPasswords('password', 'confirmPassword'),
    ],
    updateOn: 'blur',
  });
}

ngOnInit() {
}

public register() {
  const registerFormValue = { ...this.registerForm.value };
  delete registerFormValue.confirmPassword;

  const { keepMeLoggedIn, ...registerUser } = registerFormValue;

  this.authService.register({ ...registerUser, avatar: this.selectedAvatar })
    .subscribe(
      (_) => {

        this.notificationService.success(`Registration successful!`);
        const user = this.registerForm.value;
        delete user.email;

        // then login
        this.authService.logIn(this.registerForm.value)
        .subscribe(
          () => {
            this.notificationService.success(`Login successful!`);
            this.router.navigate(['/home']);
          },
          () => this.notificationService.error(`Invalid email/password!`),
        );

      },
      (error) => {
        // fail silently;

        this.notificationService.error(error.error.message);

      },
    );
}


}
