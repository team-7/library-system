# **Lybrary System Project**


## What our project do

Our system is open for public use but its resticted to the landing page, registered users and admins.

## NotLogged User can:
- Login;
- Register;

## User can:
- View all books
- View individual book
- Borrow/return a book
- Collect gaming points
- Rate a book
- Create review
- Vote review
- Flag review
- Improve/reduce karma points (reputation)
- Edit his reviews
- Edit his profile-info
- Change password
- Delete account

## Admin can:
- User functionalities
- Create book
- View all users
- Ban/Unban user
- Make admin/Fire admin

> Our URL Routing:

| Routes                            |    Admin       |    ViewUser       |    Public   |     
| :---                              |     :---:      |         :---: |         ---: |
| /landing                          |   ✘           |    ✘          |     ✔️        |
| /home                         |   ✔️           |    ✔️          |     ✔️        |
| /users/:id                            |   ✔️           |    ✔️          |     ✘        |
| /users/:id/edit-info                          |   ✔️           |    ✘          |     ✘        |
| /users/:id/edit-password                      |   ✔️           |    ✘          |     ✘        |
| /books                     |   ✔️           |    ✔️          |     ✘        |
| /books/:id                            |   ✔️           |    ✔️          |     ✘        |
| /books/create                        |   ✔️           |    ✘          |     ✘        |
| /users                 |   ✔️           |    ✘          |     ✘        |


## Getting Started

If you want to test and run the project on your local machine you need to clone repository into a brand new folder. Follow the steps:

1. Follow the link: [LibrarySystem](https://gitlab.com/team-7/library-system) and clone the project;
2. Open front-end folder and run `npm install` command then the same for back-end;
5. To start the backEnd project run `npm run start:dev` command;
6. To start the frontEnd project and open it directly in browser run `npm start` command;

For more information you can open package.json file in the project and check different commands in scripts property - "scripts".
