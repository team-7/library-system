import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { JWTPayload } from '../auth/models/jwtPayload.dto';
import { User } from '../common/decorators/index';
import { AccessGuard, AuthGuardAdmin, NotLoggedGuard } from '../common/guards';
import { ChangePasswordDTO } from './models/change-password.dto';
import { RegisterUserDto } from './models/register-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { UpdateUserDTO } from './models/update-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('query')
  async userExists(
    @Query('username') username: string,
    @Query('email') email: string,
  ): Promise<ShowUserDTO> {

    return this.usersService.userExists({ username, email});
  }

  @UseGuards(AccessGuard)
  @Get()
  async allBy(
    @User() user: JWTPayload,
    @Query('email') searchEmail?: string,
    @Query('username') searchUsername?: string,
  ): Promise<ShowUserDTO[]> {
    return await this.usersService.allBy({
     email: searchEmail,
     username: searchUsername,
    }, user);
  }

  @UseGuards(AccessGuard)
  @Get(':id')
  async getById(
    @Param('id') id: string,
  ): Promise<ShowUserDTO> {
    return this.usersService.findById(id);
  }

  @Get(':id/borrowed')
  async getUsersBooks(
    @Param('id') id: string,
  ) {

    return this.usersService.getUsersBooks(id);
  }

  @UseGuards(NotLoggedGuard)
  @Post()
  async addUser(
    @Body(new ValidationPipe({
      whitelist: false,
      transform: true,
    }),
    ) body: RegisterUserDto,
    ): Promise<ShowUserDTO> {

      return await this.usersService.addUser(body);
  }

  @UseGuards(AccessGuard)
  @Patch('password')
  async changePassword(
    @User() user: JWTPayload,
    @Body() body: ChangePasswordDTO,
  ): Promise<ShowUserDTO> {

    return await this.usersService.changePass(user, body.currPassword, body.newPassword);
  }

  @UseGuards(AccessGuard)
  @Patch(':id')
  async updateUser(
    @User() user: JWTPayload,
    @Body(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      }),
    ) body: UpdateUserDTO,
    @Param('id') userId: string,
    ): Promise<ShowUserDTO> {
    return this.usersService.updateUser(userId, body, user);
  }

  @UseGuards(AccessGuard)
  @Delete(':id')
  async deleteUser(
    @User() user: JWTPayload,
  ): Promise<ShowUserDTO> {

    return await this.usersService.deleteUser(user);
  }

  @UseGuards(AuthGuardAdmin)
  @Put(':id/privileges')
  async changePrivileges(
    @Body() roles: string[],
    @Param('id') userId: string,
  ): Promise<ShowUserDTO> {

    return await this.usersService.changePrivileges(userId, roles);
  }

  @UseGuards(AuthGuardAdmin)
  @Get(':id/ban')
  async banUser(
    @Param('id') userId: string,
  ): Promise<ShowUserDTO> {

    return await this.usersService.banUser(userId);
  }

  @UseGuards(AuthGuardAdmin)
  @Get(':id/unban')
  async unbanUser(
    @Param('id') userId: string,
  ): Promise<ShowUserDTO> {

    return await this.usersService.unbanUser(userId);
  }

}
