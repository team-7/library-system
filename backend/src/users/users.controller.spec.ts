import { Test, TestingModule } from '@nestjs/testing';
import { JWTPayload } from '../auth/models';
import { UpdateUserDTO } from './models/update-user.dto';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('Users Controller', () => {
    let controller: UsersController;
    const user: JWTPayload = {username: 'Martin', id: 'uuid', roles: ['Admin', 'User'], banned: false};
    const usersService = {
        allBy() {/* empty */},
        findById() {/* empty */},
        addUser() {/* empty */},
        changePass() {/* empty */},
        updateUser() {/* empty */},
        deleteUser() {/* empty */},
        changePrivileges() {/* empty */},
        isUserBanned() {/* empty */},
        banUser() { /* empty */},
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            controllers: [UsersController],
            providers: [
                {
                    provide: UsersService,
                    useValue: usersService,
                },
            ],
        }).compile();

        controller = module.get<UsersController>(UsersController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();

    });

    describe('usersService.allBy should', () => {
        const username: string = 'Martin';
        const email: string = 'martin@gmail.com';

        it('should call allBy with no params', async () => {
            const spy = jest.spyOn(usersService, 'allBy');
            await controller.allBy(user);

            expect(usersService.allBy).toHaveBeenCalledWith({username: undefined, email: undefined}, user);
        });

        it('should call allBy with with both params', async () => {
            const spy = jest.spyOn(usersService, 'allBy');

            await controller.allBy(user, username, email);

            expect(usersService.allBy).toHaveBeenCalledWith({ username, email }, user);
        });

        it('should call allBy with with username param', async () => {
            const spy = jest.spyOn(usersService, 'allBy');

            await controller.allBy(user, username, undefined);

            expect(usersService.allBy).toHaveBeenCalledWith({ username, email: undefined }, user);
        });

        it('should call allBy with with email param', async () => {
            const spy = jest.spyOn(usersService, 'allBy');

            await controller.allBy(user, undefined, email);

            expect(usersService.allBy).toHaveBeenCalledWith({ username: undefined, email }, user);
        });

        it('should return result', async () => {
            const spy = jest.spyOn(usersService, 'allBy').mockImplementation(() => 'test');

            const response = await controller.allBy(user, username, email);

            expect(response).toEqual('test');
        });

    });
    describe('getById should', () => {

        it('call usersService.getById exactly once', async () => {
            const spy = jest.spyOn(usersService, 'findById');

            await controller.getById('uuid');

            expect(usersService.findById).toHaveBeenCalledTimes(1);
        });

        it('call usersService.getById with id string', async () => {
            const spy = jest.spyOn(usersService, 'findById');

            await controller.getById('uuid');

            expect(usersService.findById).toHaveBeenCalledWith('uuid');

        });

        it('call usersService.getById and return value', async () => {
            const spy = jest.spyOn(usersService, 'findById').mockImplementation(() => 'test');

            const response = await controller.getById('uuid');

            expect(response).toEqual('test');
        });
    });

    describe('addUser should', () => {

        it('call usersService.addUser exactly once', async () => {
            const spy = jest.spyOn(usersService, 'addUser');
            const newUser = { username: 'Martin', email: 'martin@gmail.com', password: '12345678'};
            await controller.addUser(newUser);

            expect(usersService.addUser).toHaveBeenCalledTimes(1);
        });

        it('call usersService.addUser with username and password', async () => {
            const spy = jest.spyOn(usersService, 'addUser');
            const newUser = { username: 'Martin', email: 'martin@gmail.com', password: '12345678'};
            await controller.addUser(newUser);

            expect(usersService.addUser).toHaveBeenCalledWith(newUser);
        });

        it('call usersService.addUser and return value', async () => {
            const spy = jest.spyOn(usersService, 'addUser').mockImplementation(() => 'test');
            const newUser = { username: 'Martin', email: 'martin@gmail.com', password: '12345678'};
            const response = await controller.addUser(newUser);

            expect(response).toEqual('test');
        });

    });

    describe('changePassword should', () => {

        it('call usersService.changePass exactly once', async () => {
            const spy = jest.spyOn(usersService, 'changePass');
            await controller.changePassword(user, { currPass: '12345678', newPass: '10101010'});

            expect(usersService.changePass).toHaveBeenCalledTimes(1);
        });

        it('call usersService.changePass with params', async () => {
            const spy = jest.spyOn(usersService, 'changePass');
            await controller.changePassword(user, { currPass: '12345678', newPass: '10101010'});

            expect(usersService.changePass).toHaveBeenCalledWith(user, '12345678', '10101010');
        });

        it('call usersService.changePass and return value', async () => {
            const spy = jest.spyOn(usersService, 'changePass').mockImplementation(() => 'test');
            const response = await controller.changePassword(user, { currPass: '12345678', newPass: '10101010'});

            expect(response).toEqual('test');
        });
    });

    describe('updateUser should', () => {

        let fullUpdateObj: UpdateUserDTO;
        beforeEach(() => {
            fullUpdateObj = { username: 'Martin Stoykov', email: 'martin46@gmail.com'};
        });

        it('call usersService.updateUser exactly once', async () => {
            const spy = jest.spyOn(usersService, 'updateUser');
            await controller.updateUser(user, fullUpdateObj, 'userUUID');

            expect(usersService.updateUser).toHaveBeenCalledTimes(1);
        });

        it('call usersService.updateUser with both username and email', async () => {
            const spy = jest.spyOn(usersService, 'updateUser');
            await controller.updateUser(user, fullUpdateObj, 'userUUID');

            expect(usersService.updateUser).toHaveBeenCalledWith('userUUID', fullUpdateObj, user);
        });

        it('call usersService.updateUser with username only', async () => {
            const spy = jest.spyOn(usersService, 'updateUser');
            delete fullUpdateObj.email;

            await controller.updateUser(user, fullUpdateObj, 'userUUID');

            expect(usersService.updateUser).toHaveBeenCalledWith('userUUID', fullUpdateObj, user);
        });

        it('call usersService.updateUser with email only', async () => {
            const spy = jest.spyOn(usersService, 'updateUser');
            delete fullUpdateObj.username;
            await controller.updateUser(user, fullUpdateObj, 'userUUID');

            expect(usersService.updateUser).toHaveBeenCalledWith('userUUID', fullUpdateObj, user);
        });

        it('call usersService.updateUser with no updates', async () => {
            const spy = jest.spyOn(usersService, 'updateUser');
            delete fullUpdateObj.username;
            await controller.updateUser(user, {}, 'userUUID');

            expect(usersService.updateUser).toHaveBeenCalledWith('userUUID', {}, user);
        });

        it('call usersService.updateUser and return value', async () => {
            const spy = jest.spyOn(usersService, 'updateUser').mockImplementation(() => 'test');
            const response = await controller.updateUser(user, fullUpdateObj, 'userUUID');

            expect(response).toEqual('test');
        });

    });

    describe('deleteUser should', () => {

        const deleteId = 'userUUID';

        it('call usersService.deleteUser exactly once', async () => {
            const spy = jest.spyOn(usersService, 'deleteUser');
            await controller.deleteUser(user, deleteId);

            expect(usersService.deleteUser).toHaveBeenCalledTimes(1);
        });

        it('call usersService.deleteUser with both username and email', async () => {
            const spy = jest.spyOn(usersService, 'deleteUser');
            await controller.deleteUser(user, deleteId);

            expect(usersService.deleteUser).toHaveBeenCalledWith(user, deleteId);
        });

        it('call usersService.deleteUser with username only', async () => {
            const spy = jest.spyOn(usersService, 'deleteUser').mockImplementation(() => 'test');

            const response = await controller.deleteUser(user, deleteId);

            expect(response).toEqual('test');
        });
    });

    describe('changePrivileges should', () => {

            it('changePrivileges should call usersService.changePrivileges', async () => {
                const spy = jest.spyOn(usersService, 'changePrivileges');
                await controller.changePrivileges(['Admin', 'User'], 'userUUID');
                expect(usersService.changePrivileges).toHaveReturnedTimes(1);
                spy.mockClear();
            });

            it('changePrivileges should call usersService.changePrivileges', async () => {
                const spy = jest.spyOn(usersService, 'changePrivileges');
                await controller.changePrivileges(['Admin', 'User'], 'userUUID');
                expect(usersService.changePrivileges).toHaveBeenCalledWith('userUUID', ['Admin', 'User']);
                spy.mockClear();
            });

            it('changePrivileges should call usersService.changePrivileges', async () => {
                const spy = jest.spyOn(usersService, 'changePrivileges').mockImplementation(() => 'test');
                const response = await controller.changePrivileges(['Admin', 'User'], 'userUUID');
                expect(response).toEqual('test');
                spy.mockClear();
            });
        });

    describe('isUserBanned should', () => {

            it('isUserBanned should call usersService.isUserBanned', async () => {
                const spy = jest.spyOn(usersService, 'isUserBanned');
                await controller.isUserBanned(user, 'userUUID');
                expect(usersService.isUserBanned).toHaveBeenCalledTimes(1);
                spy.mockClear();
            });

            it('isUserBanned should call usersService.isUserBanned', async () => {
                const spy = jest.spyOn(usersService, 'isUserBanned');
                await controller.isUserBanned(user, 'userUUID');
                expect(usersService.isUserBanned).toHaveBeenCalledWith(user, 'userUUID');
                spy.mockClear();
            });

            it('isUserBanned should call usersService.isUserBanned', async () => {
                const spy = jest.spyOn(usersService, 'isUserBanned').mockImplementation(() => 'test');
                const response = await controller.isUserBanned(user, 'userUUID');
                expect(response).toEqual('test');
                spy.mockClear();
            });
        });

    describe('banUser should', () => {
            it('banUser should call usersService.banUser', async () => {
                const spy = jest.spyOn(usersService, 'banUser');
                await controller.banUser('userUUID');
                expect(usersService.banUser).toHaveBeenCalledTimes(1);
                spy.mockClear();
                });

            it('banUser should call usersService.banUser', async () => {
                    const spy = jest.spyOn(usersService, 'banUser');
                    await controller.banUser('userUUID');
                    expect(usersService.banUser).toHaveBeenCalledWith('userUUID');
                    spy.mockClear();
                    });

            it('banUser should call usersService.banUser', async () => {
                        const spy = jest.spyOn(usersService, 'banUser').mockImplementation(() => 'test');
                        const response = await controller.banUser('userUUID');
                        expect(response).toEqual('test');
                        spy.mockClear();
                        });
        });

});
