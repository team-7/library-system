import { Expose, Transform } from 'class-transformer';
import { Levels } from '../enum';
import { UserRating } from '../enum/user-rating.enum';

export class ShowUserDTO {

  @Expose()
  public id: string;

  @Expose()
  public username: string;

  @Expose()
  public email: string;

  @Expose()
  public avatar: string;

  @Expose()
  public gamingp: number;

  @Expose()
  @Transform((_, obj) => obj.level = obj.gamingp > 300 ? Levels.expert : (obj.gamingp > 100 ? Levels.seasoned : Levels.rookie))
  public level: Levels;

  @Expose()
  public karmap: number;

  @Expose()
  @Transform((_, obj) => obj.rating = obj.karmap >= -3 && obj.karmap <= 3 ?
  UserRating.Neutral : (obj.karmap > 3 ? UserRating.Positive : UserRating.Negative ))
  public rating: UserRating;

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.name))
  public roles: string[];

  @Expose()
  public banned: boolean;
}
