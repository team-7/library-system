export class ChangePasswordDTO {
    currPassword: string;
    newPassword: string;
}
