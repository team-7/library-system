import { IsEmail, IsString, Length } from 'class-validator';

export class RegisterUserDto {

  avatarUrl: string;

  @IsEmail()
  email: string;

  @IsString()
  @Length(5, 20)
  username: string;

  @IsString()
  @Length(8)
  password: string;
}
