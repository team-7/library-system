import { Expose } from 'class-transformer';
import { IsEmail, IsOptional, IsString, Length } from 'class-validator';

export class UpdateUserDTO {

    @Expose()
    @IsOptional()
    @IsString()
    @Length(5, 20)
    username?: string;

    @Expose()
    @IsOptional()
    @IsEmail()
    email?: string;

}
