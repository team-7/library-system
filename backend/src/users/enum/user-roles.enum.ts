export enum Roles {
  User = 'User',
  Admin = 'Admin',
  Administrator = 'Administrator',
}
