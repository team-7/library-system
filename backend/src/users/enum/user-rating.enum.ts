export enum UserRating {
    Neutral = 'neutral',
    Positive = 'positive',
    Negative = 'negative',
}
