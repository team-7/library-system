export enum Levels {
   rookie = 'rookie',
   seasoned = 'seasoned',
   expert = 'expert',
}
