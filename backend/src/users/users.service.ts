import { BadRequestException, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { JWTPayload } from '../auth/models/jwtPayload.dto';
import { LoginDTO } from '../auth/models/login.dto';
import { ShowBookDTO } from '../books/models/showBookDTO';
import { UserOperationError } from '../common/exceptions/users/users.exception';
import { Logger } from '../common/logger';
import { Book } from '../database/entities/books.entity';
import { Role } from '../database/entities/role.entity';
import { User } from '../database/entities/user.entity';
import { Roles } from './enum/user-roles.enum';
import { CreateRoleDTO } from './models/create-role.dto';
import { RegisterUserDto } from './models/register-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { UpdateUserDTO } from './models/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Role) private readonly roleRepository: Repository<Role>,
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    @Inject(Logger) private readonly logger: Logger,
  ) { }

  async allBy(queryObj: UpdateUserDTO, user: JWTPayload): Promise<ShowUserDTO[]> {
    this.stripUndefined(queryObj, UpdateUserDTO);

    if (!this.isAdmin(user.roles) && Object.entries(queryObj).length === 0) {
      throw new BadRequestException('Users are not permited to get list of all users');
    }
    const allUsers: User[] = await this.userRepository.find({ where: { ...queryObj, isDeleted: false } });

    return plainToClass(ShowUserDTO, allUsers, {
      excludeExtraneousValues: true,
    });

  }

  async userExists(queryObj: UpdateUserDTO): Promise<ShowUserDTO> {

    if (queryObj.username === 'null') {
      delete queryObj.username;
    }

    if (queryObj.email === 'null') {
      delete queryObj.email;
    }

    if (queryObj === {}) {
      throw new BadRequestException();
    }

    const foundUser: User = await this.userRepository.findOne({ where:
      queryObj,
    });

    const user: User = !foundUser ? null : foundUser;

    return plainToClass(ShowUserDTO, user, {
      excludeExtraneousValues: true,
    });
  }

  async emailExists(emailToFind: string): Promise<ShowUserDTO> {

    const foundUser: User = await this.userRepository.findOne({ where: {
      email: emailToFind,
    }});

    const user: User = !foundUser ? null : foundUser;

    return plainToClass(ShowUserDTO, user, {
      excludeExtraneousValues: true,
    });
  }

  public async findById(id: string): Promise<ShowUserDTO> {
    const user: User = await this.userRepository.findOne({ where: { id, isDeleted: false } });
    if (!user) {
      throw new UserOperationError('User not found!', 404, undefined);
    }

    return plainToClass(ShowUserDTO, {...user}, {
      excludeExtraneousValues: true,
    });

  }

  public async getUsersBooks(id: string) {
    const user: User = await this.userRepository.findOne({ where: { id, isDeleted: false } });

    if (!user) {
      throw new UserOperationError('User not found!', 404, undefined);
    }

    const books = await user.books;

    return plainToClass(ShowBookDTO, books, {
      excludeExtraneousValues: true,
    });
  }

  public async addUser(user: RegisterUserDto): Promise<ShowUserDTO> {
    const role = await this.roleRepository.findOne({
      name: Roles.User,
    });

    if (!role) {
      throw new BadRequestException(`No user role!`);
    }

    const foundUser: User = await this.userRepository.findOne({ where: { username: user.username, isDeleted: false }});
    if (foundUser) {
      throw new UserOperationError('User already exists', 401, undefined);
    }

    const createdUser = this.userRepository.create(user);

    createdUser.password = await bcrypt.hash(user.password, 10);
    createdUser.roles = [role];

    let savedUser: User;
    try {
      savedUser = await this.userRepository.save(createdUser);
    } catch (error) {
      throw new UserOperationError(error, 500, undefined);
    }

    await this.logger.makeDirOnUserCreation(savedUser.username);

    return plainToClass(ShowUserDTO, savedUser, {
      excludeExtraneousValues: true,
    });

  }

  async changePass(user: JWTPayload, oldPass: string, newPass: string): Promise<ShowUserDTO> {
    const foundUser: User = await this.findByUsername(user.username);

    if (!(await bcrypt.compare(oldPass, foundUser.password))) {
      throw new BadRequestException('Invalid password');
    }

    if (await bcrypt.compare(newPass, foundUser.password)) {
      throw new BadRequestException('New password must be different than the old one');
    }

    foundUser.password = await bcrypt.hash(newPass, 10);
    let updatedUser: User;
    try {
      updatedUser = await this.userRepository.save(foundUser);
    } catch (error) {
      throw new UserOperationError(error.message, 400, foundUser.username);
    }

    return plainToClass(ShowUserDTO, updatedUser, {
        excludeExtraneousValues: true,
      });

  }

  async updateUser(updUserId: string, propsToUpd: UpdateUserDTO, user: JWTPayload) {

    if (!this.isAdmin(user.roles) && updUserId !== user.id) {
      throw new BadRequestException('Only admins can change others data');
    }

    const foundUser = await this.userRepository.findOne({ where: { id: updUserId, isDeleted: false } });

    if (!foundUser) {
      throw new UnauthorizedException();
    }

    this.stripUndefined(propsToUpd, UpdateUserDTO);

    if (Object.keys(propsToUpd).length === 0) {
      throw new UserOperationError('Nothing to update', 400, user.username);
    }

    const userUpdated = {
      ...foundUser,
      ...propsToUpd,
    };
    let updatedUser: User;
    try {
      updatedUser = await this.userRepository.save(userUpdated);
    } catch (error) {
      throw new BadRequestException('Unhandled db error');
    }

    return plainToClass(ShowUserDTO, updatedUser, {
      excludeExtraneousValues: true,
    });
  }

  async deleteUser(user: JWTPayload): Promise<ShowUserDTO> {

    const foundUser: User = await this.userRepository.findOne({ where: { id: user.id, isDeleted: false } });
    if (!foundUser || foundUser.isDeleted) {
      throw new BadRequestException('User not found');
    }

    const foundBooks: Book[] = await foundUser.books;

    foundBooks.map(async el => {
      el.user = null;
      el.borrowed = false;
      return el;
    });

    await this.bookRepository.save(foundBooks);

    foundUser.isDeleted = true;
    const deletedUser = await this.userRepository.save(foundUser);

    return plainToClass(ShowUserDTO, deletedUser, {
      excludeExtraneousValues: true,
    });
  }

  async changePrivileges(userId: string, role: string[]): Promise<ShowUserDTO> {
    const foundUser: User = await this.userRepository.findOne({ where: { id: userId, isDeleted: false } });

    if (!foundUser) {
      throw new BadRequestException();
    }
    const rolesToSearch = role.reduce((acc, curr) => {
      acc.push({ name: curr });
      return acc;
    }, []);
    const roles: Role[] = await this.roleRepository.find({ where: rolesToSearch });

    foundUser.roles = roles;
    await this.userRepository.save(foundUser);
    return plainToClass(ShowUserDTO, foundUser, {
      excludeExtraneousValues: true,
    });
  }

  async banUser(userId: string): Promise<ShowUserDTO> {
    const foundUser: User = await this.userRepository.findOne({relations: ['reviews', 'books'] , where : { id: userId, isDeleted: false} });

    if (!foundUser) {
      throw new BadRequestException();
    }

    const foundBooks: Book[] = await foundUser.books;

    foundBooks.map(async el => {
      el.user = null;
      el.borrowed = false;
      return el;
    });

    await this.bookRepository.save(foundBooks);

    foundUser.banned = true;

    const updatedUser: User = await this.userRepository.save(foundUser);

    if (!updatedUser) {
      throw new BadRequestException('Failed to ban user');
    }

    return plainToClass(ShowUserDTO, updatedUser, {
      excludeExtraneousValues: true,
    });

  }

  async unbanUser(id: string) {
    const foundUser: User = await this.userRepository.findOne({ where: { id }});

    if (!foundUser) {
      throw new BadRequestException('Cant unban unexisting user!');
    }

    foundUser.banned = false;

    try {
      await this.userRepository.save(foundUser);
      return plainToClass(ShowUserDTO, foundUser, {
        excludeExtraneousValues: true,
      });
    } catch (error) {
      throw new BadRequestException('Couldnt update the user!');
    }
  }

  async createRole(role: CreateRoleDTO): Promise<Role> {
    return await this.roleRepository.save(role);
  }

  async authenticateLogin(user: LoginDTO): Promise<JWTPayload> {
    const foundUser: User = await this.findByUsername(user.username);

    if (!foundUser) {
      throw new BadRequestException('User with that username do not exist!');
    }

    if (foundUser.banned) {
      throw new BadRequestException('User is banned.');
    }

    if (await bcrypt.compare(user.password, foundUser.password)) {
      const { id, username, banned } = foundUser;
      const roles = foundUser.roles.map((el: any) => el.name);
      return { id, username, roles, banned };
    }

    throw new BadRequestException('Entered password is invalid!');
  }

  private isAdmin(roles: string[]): boolean {
    return roles.includes('Admin');
  }

  private async findByUsername(username: string): Promise<User> {

    const foundUser = await this.userRepository.findOne({ where: { username, isDeleted: false } });

    if (!foundUser) {
      throw new UserOperationError('User not found', 404, undefined);
    }

    return foundUser;
  }

  private stripUndefined(obj: any, dto: any): void {
    for (const key in obj) {
      if (typeof obj[key] === 'undefined' || Object.keys(dto).includes(key)) {
        delete obj[key];
      }
    }
  }

}
