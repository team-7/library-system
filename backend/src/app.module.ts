import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { RouterModule } from 'nest-router';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BooksModule } from './books/books.module';
import { GlobalExceptionFilter } from './common/filters/global.filter';
import { Logger } from './common/logger';
import { CoreModule } from './core/core.module';
import { DatabaseModule } from './database/database.module';
import { FlagsModule } from './flags/flags.module';
import { RateModule } from './rate/rate.module';
import { ReviewsModule } from './reviews/review.modul';
import { routes } from './routes';
import { UsersModule } from './users/users.module';
import { VoteModule } from './votes/votes.module';

@Module({
  imports: [
    RouterModule.forRoutes(routes),
    ReviewsModule,
    DatabaseModule,
    UsersModule,
    BooksModule,
    VoteModule,
    CoreModule,
    FlagsModule,
    RateModule,
  ],
  controllers: [AppController],
  providers: [AppService, { provide: APP_FILTER, useClass: GlobalExceptionFilter}, Logger],
})
export class AppModule {}
