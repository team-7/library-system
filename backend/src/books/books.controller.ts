import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Put, UploadedFile, UseGuards, UseInterceptors, ValidationPipe } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { User } from '../common/decorators';
import { JWTPayload } from './../auth/models/jwtPayload.dto';
import { AccessGuard } from './../common/guards/jwtAccess.guard';
import { BooksService } from './books.service';
import { CreateBookDTO } from './models/createBookDTO';
import { ResponseMessageDTO } from './models/responseMessageDTO';
import { ShowBookDTO } from './models/showBookDTO';
import { UpdateBookDTO } from './models/updateBookDTO';

@Controller('books')
export class BooksController {
  public constructor(private readonly service: BooksService) {}

  @UseGuards(AccessGuard)
  @Get()
  @HttpCode(HttpStatus.OK)
  public async allBooks(
    @User() user: JWTPayload,
  ): Promise<ShowBookDTO[]> {
    return await this.service.allBooks(user);
  }

  @UseGuards(AccessGuard)
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  public async specificBook(
    @User() user: JWTPayload,
    @Param('id') id: string): Promise<ShowBookDTO> {

    return await this.service.specificBook(id, user);
  }

  @UseGuards(AccessGuard)
  @Put(':id/state')
  @HttpCode(HttpStatus.OK)
  public async changeBookState(
    @User() user: JWTPayload,
    @Param('id') bookID: string,
    @Body() body: UpdateBookDTO,
  ): Promise<ShowBookDTO> {
    const a = await this.service.changeBookState(bookID, user, body);
    return a;
  }

  // @UseGuards(AccessGuard)
  // @Get(':id/return')
  // @HttpCode(HttpStatus.OK)
  // public async returnBook(
  //   @User() user: JWTPayload,
  //   @Param() bookID: string,
  // ): Promise<ResponseMessageDTO> {
  //   try {
  //     await this.service.returnBook(bookID, user);
  //     return { msg: 'Book Updated' };
  //   } catch (error) {
  //     throw new Error('The book is borrowed!');
  //   }
  // }

  @UseGuards(AccessGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async createBook(
    @Body(new ValidationPipe({whitelist: true, transform: true})) body: CreateBookDTO,
    @User() user: JWTPayload,
  ): Promise<ShowBookDTO> {

    return await this.service.createBook(body, user);
  }

  @UseGuards(AccessGuard)
  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  public async deleteBook(
    @User() user: JWTPayload,
    @Param() id: string): Promise<ResponseMessageDTO> {
    await this.service.deleteBook(id, user);
    return { msg: 'Book deleted' };
  }

  @Put('/:id/booksImg')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(FileInterceptor('file'))
  public async uploadUserAvatar(
    @Param('id') id: string,
    @UploadedFile() file: any,
  ): Promise<ShowBookDTO> {
    const updateBookProperties: Partial<UpdateBookDTO> = {
      bookImgUrl: file.filename,
    };

    return await this.service.updateBookUrl(id, updateBookProperties);
  }

}
