import { BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { GlobalException } from '../common/exceptions/global.exception';
import { JWTPayload } from './../auth/models/jwtPayload.dto';
import { Book } from './../database/entities/books.entity';
import { Rate } from './../database/entities/rate.entity';
import { User } from './../database/entities/user.entity';
import { CreateBookDTO } from './models/createBookDTO';
import { ShowBookDTO } from './models/showBookDTO';
import { UpdateBookDTO } from './models/updateBookDTO';

export class BooksService {
  public constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Book) private readonly bookRepo: Repository<Book>,
    @InjectRepository(Rate) private readonly rateRepo: Repository<Rate>,
  ) {}

  public async allBooks(user: JWTPayload, withUnlisted: boolean = false, withDeleted: boolean = false): Promise<ShowBookDTO[]> {
    let books: any;

    if (user.roles.includes('Admin')) {

      books = (await this.bookRepo.find()).map(async (o) => {
        const avg = await this.getAvgRating(o.id);
        return {
          id: o.id,
          name: o.name,
          author: o.author,
          text: o.text,
          borrowed: o.borrowed,
          bookRating: avg,
          bookImgUrl: o.bookImgUrl,
          isDeleted: o.isDeleted,
        };
      });

    } else {

      books = (await this.bookRepo.find({ where: { unlisted: withUnlisted, isDeleted: withDeleted  } })).map(async (o) => {
        const avg = await this.getAvgRating(o.id);
        return {
          id: o.id,
          name: o.name,
          author: o.author,
          text: o.text,
          borrowed: o.borrowed,
          bookRating: avg,
          bookImgUrl: o.bookImgUrl,
          isDeleted: o.isDeleted,
        };
      });
    }
    const plainBooks = await Promise.all(books);
    return plainToClass(ShowBookDTO, plainBooks, {
      excludeExtraneousValues: true,
    });
  }

  public async specificBook(bookID: string, user: JWTPayload): Promise<ShowBookDTO> {
    const foundBook: Book = await this.searchBookById(bookID);

    if (!foundBook) {
      throw new BadRequestException('Can\'t access the book');
    }

    return plainToClass(ShowBookDTO, {...foundBook}, {
      excludeExtraneousValues: true,
    });
  }

  public async changeBookState(
    bookID: string,
    user: JWTPayload,
    updateBookDTO: UpdateBookDTO,
  ): Promise<ShowBookDTO> {
    const foundBook: Book = await this.searchBookById(bookID);

    if (foundBook.borrowed && !updateBookDTO.borrowed ) { //// return book

      const foundUser: User = await foundBook.user;

      if (!foundUser) {
        throw new BadRequestException('No user borrowed that book!');
      }

      if (foundUser.id !== user.id) {
        throw new BadRequestException('You cant return a book borrowed by another user!');
      }

      foundUser.gamingp += 10;
      foundBook.borrowed = false;
      foundBook.user = null;

      this.userRepo.save(foundUser);

      return plainToClass(ShowBookDTO, {...await this.bookRepo.save(foundBook)}, {
        excludeExtraneousValues: true,
      });
    } else if (!foundBook.borrowed && updateBookDTO.borrowed) { //// borrow book

      if (!foundBook || foundBook.borrowed || foundBook.unlisted) {
        throw new BadRequestException('Can\'t access the book');
      }

      const foundUser: User = await this.userRepo.findOne({ where: { id: user.id }});

      if (!foundUser) {
        throw new BadRequestException('Invalid user!');
      }

      foundBook.borrowed = true;
      foundBook.user = Promise.resolve(foundUser);

      await this.bookRepo.save(foundBook);
      return plainToClass(ShowBookDTO, foundBook, {
        excludeExtraneousValues: true,
      });
    } else if (!foundBook.unlisted && updateBookDTO.unlisted) { //// unlist book
      if (!foundBook || foundBook.borrowed || foundBook.unlisted || !user.roles.includes('Admin')) {
        throw new GlobalException('Can\'t unlist the book', 404, user.username);
      }

      const foundUser: User = await this.userRepo.findOne({ where: { id: user.id }});

      if (!foundUser) {
        throw new GlobalException('Invalid user!', 404, user.username);
      }

      foundBook.unlisted = true;

      await this.bookRepo.save(foundBook);
      return plainToClass(ShowBookDTO, foundBook, {
        excludeExtraneousValues: true,
      });
    } else if (foundBook.unlisted && !updateBookDTO.unlisted) { //// enlist book

      foundBook.unlisted = false;

      await this.bookRepo.save(foundBook);

      return plainToClass(ShowBookDTO, {...await this.bookRepo.save(foundBook)}, {
        excludeExtraneousValues: true,
      });
    }
  }

  public async createBook(body: CreateBookDTO, user: JWTPayload): Promise<ShowBookDTO> {
    if (user.roles.includes('Admin')) {
      const bookEntity = await this.bookRepo.create(body);
      const foundUser: User = await this.userRepo.
      createQueryBuilder('user')
      .leftJoinAndSelect('user.books', 'book', 'book.borrowed = :borrowed', { borrowed: false })
      .where('user.username = :username', { username: user.username })
      .getOne();
      // bookEntity.user = Promise.resolve(foundUser);
      return plainToClass(ShowBookDTO, await this.bookRepo.save(bookEntity), {
        excludeExtraneousValues: true,
      });
    } else {
      throw new GlobalException(`Only Admin can create new Books!`, 400, user.username);
    }
  }

  public async updateBookUrl(id: string, book: Partial<UpdateBookDTO>): Promise<ShowBookDTO> {
    const bookEntity: Book = await this.searchBookById(id);

    const bookUpdated: Book = {...bookEntity, ...book};

    const savedBook: Book = await this.bookRepo.save(bookUpdated);
    return plainToClass(ShowBookDTO, savedBook, {
      excludeExtraneousValues: true,
    });

  }

  public async deleteBook(id: string, user: JWTPayload): Promise<ShowBookDTO> {

    if (user.roles.includes('Admin')) {

      const foundBook = await this.bookRepo.findOne(id);

      foundBook.isDeleted = true;

      this.bookRepo.save(foundBook);

      return plainToClass(ShowBookDTO, foundBook, {
        excludeExtraneousValues: true,
      });
    } else {
      throw new GlobalException(`Only Admin can delete Books!`, 404, user.username);
    }
  }

  private async searchBookById(bookId: string): Promise<Book> {
    const foundBook: Book = await this.bookRepo.findOne(bookId);
    if (foundBook === undefined) {
      throw new BadRequestException('The book is not avaible!');
    }

    return foundBook;
  }

  private async getAvgRating(bookId: string) {
    const bookRating = await this.rateRepo
    .createQueryBuilder()
    .where('bookId = :id', { id: bookId })
    .select('AVG(rateType)', 'avg')
    .getRawOne();

    return bookRating.avg;
  }
}
