import { Rate } from './../database/entities/rate.entity';
import { Book } from './../database/entities/books.entity';

import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { GlobalException } from '../common/exceptions/global.exception';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Module({
  imports: [TypeOrmModule.forFeature([Book, User, Rate]),
  MulterModule.register({
    fileFilter(_, file, cb) {
      const ext = extname(file.originalname);
      const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];

      if (!allowedExtensions.includes(ext)) {
        return cb(
          new GlobalException('Only images are allowed', 400, 'loggedUser'),
          false,
        );
      }

      cb(null, true);
    },
    storage: diskStorage({
      destination: './bookCovers',
      filename: (_, file, cb) => {
        const randomName = Array.from({ length: 32 })
          .map(() => Math.round(Math.random() * 10))
          .join('');

        return cb(null, `${randomName}${extname(file.originalname)}`);
      },
    }),
  }),
],
  controllers: [BooksController],
  providers: [BooksService],
})
export class BooksModule {}
