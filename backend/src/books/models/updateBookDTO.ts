import { IsBoolean, IsString } from 'class-validator';

export class UpdateBookDTO {

  @IsBoolean()
  public borrowed: boolean;

  @IsBoolean()
  public unlisted: boolean;

  @IsString()
  public bookImgUrl: string;
}
