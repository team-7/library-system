import { Expose } from 'class-transformer';
import { RateType } from 'src/rate/models/rateTypes';

export class ShowBookDTO {

    @Expose()
    public id: string;

    @Expose()
    public name: string;

    @Expose()
    public author: string;

    @Expose()
    public text: string;

    @Expose()
    public borrowed: boolean;

    @Expose()
    public unlisted: boolean;

    @Expose()
    public bookRating: number;

    @Expose()
    public bookImgUrl: string;

    @Expose() 
    public isDeleted: boolean;
}
