import { IsString, Length, IsNotEmpty } from 'class-validator';

export class CreateBookDTO {

  @IsString()
  @Length(3, 30, { message: `Length name should be between 3 and 30 characters` })
  public name: string;

  @IsString()
  @Length(3, 15, { message: `Length name of author should be between 3 and 15 characters` })
  public author: string;

  @IsString()
  @IsNotEmpty()
  public text: string;
  
}
