import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { UpdateBookDTO } from './models/updateBookDTO';

describe('AuthController Tests, ', () => {
    let controller: BooksController;
    let booksService: any;

    beforeEach(async () => {
        booksService = {
            allBooks: () => ['test', 'Shibana Kniga'],
            specificBook: () => ['Shibana knigaaa'],
            borrowBook() {
                // Nothing
             },
            returnBook() {
                // Nothing
             },
            createBook() {
                // Nothing
             },
            deleteBook() {
                // Nothing
             },
        };

        const module: TestingModule = await Test.createTestingModule({
            controllers: [BooksController],
            providers: [
                {
                    provide: BooksService,
                    useValue: booksService,
                },
            ],
        }).compile();

        controller = module.get<BooksController>(BooksController);
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(controller).toBeDefined();
    });

    describe('allBooks ', () => {

        it('should be called 1 time', async () => {
            // Arrange
            const result = ['test', 'Shibana Kniga'];
            const spy = jest.spyOn(booksService, 'allBooks').mockReturnValue(result);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            await controller.allBooks(MockUser);

            expect(spy).toBeCalledTimes(1);
        });

        it('should return array of books', async () => {
            // Arrange
            const fakeArrBooks = ['test', 'Shibana Kniga'];
            const spy = jest.spyOn(booksService, 'allBooks').mockReturnValue(fakeArrBooks);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.allBooks(MockUser);

            expect(result).toEqual(fakeArrBooks);
        });
    });

    describe('specificBook ', () => {

        it('should contain specific book', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
            };
            const spy = jest.spyOn(booksService, 'specificBook').mockReturnValue(fakeBook);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.specificBook(MockUser, '2');

            expect(result.name).toMatch(fakeBook.name);
        });

        it('should be called with the right elements', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
            };
            const spy = jest.spyOn(booksService, 'specificBook').mockReturnValue(fakeBook);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.specificBook(MockUser, '1');

            expect(spy).toBeCalledWith('1', MockUser);
        });

    });

    describe('borrowBook ', () => {

        it('should change borrowed field from false to true', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
                borrowed: false,
                unlisted: false,
            };

            const update = {
                borrowed: true,
                unlisted: false,
                bookImgUrl: null
            };

            const expectedResult = { ...fakeBook, update };

            const spy = jest.spyOn(booksService, 'changeBookState').mockReturnValue(expectedResult);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.changeBookState(MockUser, '1', update);

            // Assert
            expect(result).toEqual(expectedResult);
        });

        it('should be called with the right elements', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
                borrowed: false,
                unlisted: false,
            };

            const update = {
                borrowed: true,
                unlisted: false,
                bookImgUrl: null
            };

            const expectedResult = { ...fakeBook, update };

            const spy = jest.spyOn(booksService, 'changeBookState').mockReturnValue(expectedResult);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.changeBookState(MockUser, '1', update);

            // Assert
            expect(spy).toBeCalledWith('1', update, MockUser);
        });

    });

    describe('returnBook ', () => {

        it('should change borrowed field from true to false', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
                borrowed: true,
                unlisted: false,
            };

            const update = {
                borrowed: false,
                unlisted: false,
                bookImgUrl: null
            };

            const expectedResult = { ...fakeBook, ...update };

            const spy = jest.spyOn(booksService, 'changeBookState').mockReturnValue(expectedResult);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.changeBookState(MockUser, '1', update);

            // Assert
            expect(result).toEqual({msg: 'Book Updated'});
        });

        it('should be called 1 time', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
                borrowed: true,
                unlisted: false,
            };

            const update = {
                borrowed: false,
                unlisted: false,
                bookImgUrl: null
            };

            const expectedResult = { ...fakeBook, ...update };

            const spy = jest.spyOn(booksService, 'changeBookState').mockReturnValue(expectedResult);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.changeBookState(MockUser, '1', update);

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('should be called with the right elements', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
                borrowed: true,
                unlisted: false,
            };

            const update = {
                borrowed: false,
                unlisted: false,
                bookImgUrl: null
            };

            const expectedResult = { ...fakeBook, update };

            const spy = jest.spyOn(booksService, 'changeBookState').mockReturnValue(expectedResult);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };
            const bookID = '1';

            // Act
            const result = await controller.changeBookState(MockUser, bookID, update);

            // Assert
            expect(spy).toBeCalledWith(bookID, update, MockUser);
        });

    });

    describe('createBook ', () => {

        it('should return the created book', async () => {
            // Arrange
            const createBook = {
                name: 'test',
                author: 'Pesho',
                text: 'gargata',
            };

            const spy = jest.spyOn(booksService, 'createBook').mockReturnValue(createBook);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.createBook(createBook, MockUser);

            // Assert
            expect(result).toEqual(createBook);
        });

        it('should be called 1 time', async () => {
            // Arrange
            const createBook = {
                name: 'test',
                author: 'Pesho',
                text: 'gargata',
            };

            const spy = jest.spyOn(booksService, 'createBook').mockReturnValue(createBook);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.createBook(createBook, MockUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('should be called with the right elements', async () => {
            // Arrange
            const createBook = {
                name: 'test',
                author: 'Pesho',
                text: 'gargata',
            };

            const spy = jest.spyOn(booksService, 'createBook').mockReturnValue(createBook);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };
            const bookID = '1';

            // Act
            const result = await controller.createBook(createBook, MockUser);

            // Assert
            expect(spy).toBeCalledWith(createBook, MockUser);
        });

    });

    describe('returnBook ', () => {

        it('should change borrowed field from true to false', async () => {
             // Arrange
             const fakeBook = {
                id: '1',
                name: 'test',
                isDeleted: false,
            };

             const update = {
                isDeleted: true,
            };

             const expectedResult = { ...fakeBook, ...update };

             const spy = jest.spyOn(booksService, 'deleteBook').mockReturnValue(expectedResult);

             const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
             const result = await controller.deleteBook(MockUser, '1');

            // Assert
             expect(result).toEqual({msg: 'Book deleted'});
        });

        it('should be called 1 time', async () => {
            // Arrange
            const fakeBook = {
                id: '1',
                name: 'test',
                isDeleted: false,
            };

            const update = {
                isDeleted: true,
            };

            const expectedResult = { ...fakeBook, ...update };

            const spy = jest.spyOn(booksService, 'deleteBook').mockReturnValue(expectedResult);

            const MockUser = {
                id: '1',
                username: 'Batman',
                email: 'batman@getMaxListeners.com',
                roles: ['Admin', 'User'],
                banned: false,
            };

            // Act
            const result = await controller.deleteBook(MockUser, '1');

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('should be called with the right elements', async () => {
            // Arrange
            const fakeBook = {
               id: '1',
               name: 'test',
               isDeleted: false,
            };

            const update = {
               isDeleted: true,
            };

            const expectedResult = { ...fakeBook, ...update };

            const spy = jest.spyOn(booksService, 'deleteBook').mockReturnValue(expectedResult);

            const MockUser = {
               id: '1',
               username: 'Batman',
               email: 'batman@getMaxListeners.com',
               roles: ['Admin', 'User'],
               banned: false,
            };

            // Act
            const result = await controller.deleteBook(MockUser, '1');

            // Assert
            expect(spy).toBeCalledWith('1', MockUser);
        });

    });

});
