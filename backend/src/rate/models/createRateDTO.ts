import { RateType } from './rateTypes';
import { IsEnum } from 'class-validator';
export class CreateRateDTO {

    @IsEnum(RateType, { message: `The rate type is not from 1 to 5` })
    public rateType: RateType;
}
