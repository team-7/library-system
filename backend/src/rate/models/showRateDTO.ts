import { RateType } from './rateTypes';
import { Expose } from 'class-transformer';

export class ShowRateDTO {
    @Expose()
    public rateType: RateType;
}
