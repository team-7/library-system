import { User } from './../database/entities/user.entity';
import { Rate } from './../database/entities/rate.entity';
import { Module } from '@nestjs/common';
import { Book } from './../database/entities/books.entity';
import { RateController } from './rate.controller';
import { RateService } from './rate.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([Rate, Book, User])],
    controllers: [RateController],
    providers: [RateService],
  })
  export class RateModule {}
