import { Body, Controller, Get, Param, Post, UseGuards, ValidationPipe } from '@nestjs/common';
import { User } from '../common/decorators';
import { JWTPayload } from './../auth/models/jwtPayload.dto';
import { AccessGuard } from './../common/guards/jwtAccess.guard';
import { CreateRateDTO } from './models/createRateDTO';
import { ShowRateDTO } from './models/showRateDTO';
import { RateService } from './rate.service';

@Controller()
export class RateController {

    public constructor( private readonly service: RateService) {}

    @UseGuards(AccessGuard)
    @Post('books/:bookId/rating')
    public async createRate(
        @User() user: JWTPayload,
        @Param('bookId') bookId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: CreateRateDTO,
    ): Promise<number> {

        return await this.service.createRate(bookId, body, user);
    }

    @UseGuards(AccessGuard)
    @Get('books/:bookId/rating')
    public async getRate(
        @Param('bookId') bookId: string,
    ): Promise<ShowRateDTO[]> {
        return await this.service.rates(bookId);
    }
}
