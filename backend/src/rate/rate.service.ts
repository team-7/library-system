import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { JWTPayload } from './../auth/models/jwtPayload.dto';
import { Book } from './../database/entities/books.entity';
import { Rate } from './../database/entities/rate.entity';
import { CreateRateDTO } from './models/createRateDTO';
import { ShowRateDTO } from './models/showRateDTO';

@Injectable()
export class RateService {

    public constructor(
        @InjectRepository(Rate) private readonly rateRepo: Repository<Rate>,
        @InjectRepository(Book) private readonly bookRepo: Repository<Book>,
        @InjectRepository(User) private readonly userRepo: Repository<User>,
    ) {}

    public async createRate(bookId: string, rate: CreateRateDTO, loggedUser: JWTPayload): Promise<number> {

        const bookEntity: Book = await this.bookRepo.findOne(bookId);

        const foundUser: User = await this.userRepo
        .createQueryBuilder('users')
        .where('users.username = :username', { username: loggedUser.username })
        .getOne();

        const tempRateEnt = await this.rateRepo.findOne({ where: {
            userId: foundUser.id,
        }});

        if (tempRateEnt === undefined) {
            const rateEntity: Rate = this.rateRepo.create(rate);
            rateEntity.book = Promise.resolve(bookEntity);
            rateEntity.user = Promise.resolve(foundUser);

        } else if (foundUser.id === (await tempRateEnt.user).id && bookEntity.id === (await tempRateEnt.book).id) {
            tempRateEnt.rateType = rate.rateType;
            await this.rateRepo.save(tempRateEnt);

        } else {
            const rateEntity: Rate = this.rateRepo.create(rate);
            rateEntity.book = Promise.resolve(bookEntity);
            rateEntity.user = Promise.resolve(foundUser);

            await this.rateRepo.save(rateEntity);

        }

        const bookRating = await this.rateRepo
        .createQueryBuilder()
        .where('bookId = :id', { id: bookId })
        .select('AVG(rateType)', 'avg')
        .getRawOne();

        bookEntity.bookRating = bookRating.avg;

        this.bookRepo.save(bookEntity);

        return bookRating.avg;
    }

    public async rates(bookId: string): Promise<ShowRateDTO[]> {

        const bookEntity: Book = await this.bookRepo.findOne(bookId, { relations: ['rate']});

        return plainToClass(ShowRateDTO, (await bookEntity.rate).map(o => ({...o})), {
            excludeExtraneousValues: true,
        });
    }
}
