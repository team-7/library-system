import { Global, Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { Logger } from '../common/logger';

@Global()
@Module({
  imports: [AuthModule],
  providers: [Logger],
  exports: [AuthModule, Logger],
})
export class CoreModule {}
