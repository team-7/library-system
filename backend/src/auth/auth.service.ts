import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserOperationError } from '../common/exceptions/users/users.exception';
import { UsersService } from '../users/users.service';
import { BlacklistService } from './blacklist.service';
import { JWTPayload } from './models/jwtPayload.dto';
import { LoginDTO } from './models/login.dto';
import { TokenDTO } from './models/token.dto';

@Injectable()
export class AuthService {

  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UsersService,
    private readonly blacklistService: BlacklistService,
  ) {}

    public async login(user: LoginDTO): Promise<TokenDTO> {
      const payload: JWTPayload = await this.userService.authenticateLogin(user);
      return {
        token: await this.jwtService.signAsync(payload),
      };
    }

    public async blacklistToken(token: TokenDTO): Promise<TokenDTO> {
        return await this.blacklistService.blacklistToken(token);
    }

    public async isTokenBlacklisted(token: TokenDTO): Promise<boolean> {
      return await this.blacklistService.isBlacklisted(token);
    }

    public async isAdmin(user: JWTPayload) {
      return user.roles.includes('Admin');
    }

    public async verifyToken(token: string) {
      try {
        await this.jwtService.verifyAsync(token.split(' ')[1]);
      } catch (error) {
        throw new UserOperationError(error.message, 401, undefined);
      }
    }

}
