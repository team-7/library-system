import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { TokenBlacklist } from '../database/entities/token-blacklist.entity';
import { TokenDTO } from './models/token.dto';

@Injectable()
export class BlacklistService {

    constructor(
        @InjectRepository(TokenBlacklist) private readonly blacklistRepo: Repository<TokenBlacklist>,
    ) {}

    public async blacklistToken(token: TokenDTO): Promise<TokenDTO> {
        const blacklisted: TokenBlacklist = await this.blacklistRepo.save(token);

        return plainToClass(TokenDTO, blacklisted, {
            excludeExtraneousValues: true,
        });
    }

    public async isBlacklisted(token: TokenDTO): Promise<boolean> {
        return (await this.getToken( token )) === undefined ? false : true;
    }

    private async getToken(token: TokenDTO) {
        return await this.blacklistRepo.findOne({ select: ['token'], where: { token } });
    }

}
