import { Body, Controller, Get, HttpCode, HttpStatus, Post, UseGuards } from '@nestjs/common';
import { Token } from '../common/decorators';
import { AccessGuard, NotLoggedGuard } from '../common/guards';
import { AuthService } from './auth.service';
import { LoginDTO } from './models/login.dto';
import { TokenDTO } from './models/token.dto';

@Controller()
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    ) {}

    @UseGuards(NotLoggedGuard)
    @Post('login')
    @HttpCode(HttpStatus.OK)
    public async login(@Body() body: LoginDTO): Promise<TokenDTO> {

        return await this.authService.login(body);
    }

    @UseGuards(AccessGuard)
    @Get('logout')
    public async logout(@Token() tokenObj: TokenDTO) {

        return await this.authService.blacklistToken(tokenObj);
    }

}
