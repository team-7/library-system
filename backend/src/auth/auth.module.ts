import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtStrategy } from '../common/guards';
import { ConfigModule, ConfigService } from '../config';
import { DatabaseModule } from '../database/database.module';
import { Book } from '../database/entities/books.entity';
import { Role } from '../database/entities/role.entity';
import { TokenBlacklist } from '../database/entities/token-blacklist.entity';
import { User } from '../database/entities/user.entity';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { BlacklistService } from './blacklist.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, TokenBlacklist, Book]),
    DatabaseModule,
    UsersModule,
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.secret,
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, UsersService, BlacklistService, ConfigService],
  exports: [AuthService, JwtModule],
})
export class AuthModule {}
