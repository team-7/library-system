import { Expose } from 'class-transformer';
import { IsArray, IsBoolean, IsNotEmpty } from 'class-validator';

export class JWTPayload {

    @Expose()
    @IsNotEmpty()
    username: string;

    @Expose()
    @IsNotEmpty()
    id: string;

    @Expose()
    @IsArray()
    @IsNotEmpty()
    roles: string[];

    @Expose()
    @IsBoolean()
    banned: boolean;
}
