import { FlagTypes } from '../enums/flag-type.enum';
import { IsEnum } from 'class-validator';

export class CreateFlagDTO {
    @IsEnum(FlagTypes, {
        message: 'The type of the flag should be on of the following: Unappropriate, Offending, Spam, Other'
    })
    public flagType: FlagTypes;
}
