import { FlagTypes } from '../enums/flag-type.enum';
import { Expose } from 'class-transformer';

export class ShowFlagDTO {

    @Expose()
    public id: string;

    @Expose()
    public flagType: FlagTypes;

    @Expose()
    public reviewId: string;

    @Expose()
    public isChecked: boolean;
}
