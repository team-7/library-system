import { Controller, Get, HttpCode, HttpStatus, Param, Post, Body, Patch, ValidationPipe, UseGuards, Delete } from '@nestjs/common';
import { CreateFlagDTO } from './models/create-flag.dto';
import { FlagDataService } from './flags.service';
import { ShowFlagDTO } from './models/show-flag.dto';
import { ResponseMessageDTO } from '../reviews/models/response-message.dto';
import { AccessGuard } from '../common/guards';
import { User } from '../common/decorators/user.decorator';
import { AuthGuardAdmin } from '../common/guards/jwtAdmin.guard';
import { JWTPayload } from '../auth/models/jwtPayload.dto';

@Controller('books/:bookId/reviews/:reviewId/flags')
export class FlagController {

    public constructor(
        private readonly flagDataService: FlagDataService,
    ) {}

    @UseGuards(AuthGuardAdmin)
    @Get()
    @HttpCode(HttpStatus.OK)
    public async getReviewFlags(
        @User() user: JWTPayload,
        @Param('reviewId') reviewId: string,
    ): Promise<ShowFlagDTO[]> {
        return await this.flagDataService.getReviewFlags(user, reviewId);
    }

    @UseGuards(AccessGuard)
    @Post()
    @HttpCode(HttpStatus.CREATED)
    public async createFlag(
        @User() user: JWTPayload,
        @Body(new ValidationPipe({whitelist: true, transform: true})) newFlag: CreateFlagDTO,
        @Param('reviewId') reviewId: string,
    ): Promise<ShowFlagDTO> {
        return await this.flagDataService.createFlag(user, reviewId, newFlag);
    }

    @UseGuards(AuthGuardAdmin)
    @Delete(':flagId')
    @HttpCode(HttpStatus.OK)
    public async checkFlag(
        @User() user: JWTPayload,
        @Param('reviewId') reviewId: string,
        @Param('flagId') flagId: string,
    ): Promise<ResponseMessageDTO> {
        await this.flagDataService.checkFlag(user, reviewId, flagId);
        return {message: 'The flag was checked!'};
    }
}
