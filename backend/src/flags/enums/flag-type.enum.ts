export enum FlagTypes {
    Unappropriate = 'Unappropriate',
    Offending= 'Offending',
    Spam = 'Spam',
    Other = 'Other',
}
