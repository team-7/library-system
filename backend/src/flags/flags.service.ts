import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { JWTPayload } from '../auth/models/jwtPayload.dto';
import { GlobalException } from '../common/exceptions/global.exception';
import { User } from '../database/entities/user.entity';
import { Flag } from './../database/entities/flags.entity';
import { Review } from './../database/entities/review.entity';
import { CreateFlagDTO } from './models/create-flag.dto';
import { ShowFlagDTO } from './models/show-flag.dto';

@Injectable()
export class FlagDataService {

    public constructor(
        @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
        @InjectRepository(Flag) private readonly flagRepository: Repository<Flag>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
    ) {}

    public async getReviewFlags(
        user: JWTPayload,
        reviewId: string,
        ): Promise<ShowFlagDTO[]> {
        // if (user.roles.includes('Admin')) {
        const foundReview: Review = await this.reviewRepository.findOne(reviewId, {
                relations: ['flags'],
        });
        // } else {
        //     foundReview = await this.reviewRepository.createQueryBuilder('reviews')
        //     .leftJoinAndSelect('reviews.flags', 'flag', 'flag.isChecked = :isChecked', { isChecked: false})
        //     .where('reviews.id = :id', { id: reviewId})
        //     .getOne();
        // }

        if (!foundReview) {
            throw new GlobalException('No such review!', 404, user.username);
        }

        const plainFlags = (await foundReview.flags).map(o => ({...o, reviewId}));

        return plainToClass(ShowFlagDTO, plainFlags, {
            excludeExtraneousValues: true,
        });
    }

    public async createFlag(
        user: JWTPayload,
        reviewId: string,
        newFlag: CreateFlagDTO,
        ): Promise<ShowFlagDTO> {
        const foundReview: Review = await this.reviewRepository.findOne({
            id: reviewId,
        });

        if (!foundReview) {
            throw new GlobalException(`The review doesn't exist`, 404, user.username);
        }

        const foundUser: User = await this.userRepository.findOne({
            username: user.username,
        });

        const createdFlag: Flag = this.flagRepository.create();
        createdFlag.user = Promise.resolve(foundUser);
        createdFlag.review = Promise.resolve(foundReview);
        createdFlag.flagType = newFlag.flagType;

        await this.flagRepository.save(createdFlag);

        return plainToClass(ShowFlagDTO, {...createdFlag, reviewId}, {
            excludeExtraneousValues: true,
        });
    }

    public async checkFlag(
        user: JWTPayload,
        reviewId: string,
        flagId: string,
        ): Promise<ShowFlagDTO> {
        const foundReview: Review = await this.reviewRepository.findOne({
            id: reviewId,
        });

        if (!foundReview) {
            throw new GlobalException(`The review doesn't exist`, 404, user.username);
        }

        const foundFlag: Flag = await this.flagRepository.findOne({
            id: flagId,
        });
        
        if (!foundFlag) {
            throw new GlobalException(`The flag doesn't exist!`, 404, user.username);
        }

        foundFlag.isChecked = true;
        await this.flagRepository.save(foundFlag);

        return plainToClass(ShowFlagDTO, foundFlag, {
            excludeExtraneousValues: true,
        });
    }

}
