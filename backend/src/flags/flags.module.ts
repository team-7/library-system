import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Flag } from './../database/entities/flags.entity';
import { FlagController } from './flags.controller';
import { FlagDataService } from './flags.service';
import { Review } from '../database/entities/review.entity';
import { User } from '../database/entities/user.entity';

@Module ({
    imports: [TypeOrmModule.forFeature([Review, Flag, User])],
    controllers: [FlagController],
    providers: [FlagDataService],
})
export class FlagsModule {}
