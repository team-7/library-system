import { BadRequestException, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { plainToClass } from 'class-transformer';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JWTPayload } from '../../auth/models/jwtPayload.dto';
import { ConfigService } from '../../config';
import { ShowUserDTO } from '../../users/models/show-user.dto';
import { UsersService } from '../../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly userService: UsersService,
    private readonly configService: ConfigService,
    ) {

    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.secret,
      ignoreExpiration: true,
    });
  }

  public async validate(payload: ShowUserDTO): Promise<JWTPayload> {
    const user: JWTPayload = plainToClass(JWTPayload, await this.userService.findById(payload.id), { excludeExtraneousValues: true });
    if (!user) {
      throw new BadRequestException('User do not exist. Cant be validated');
    }

    return user;
  }
}
