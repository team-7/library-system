import { BadRequestException, CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { AuthService } from '../../auth/auth.service';
import { ShowUserDTO } from '../../users/models/show-user.dto';

@Injectable()
export class AccessGuard extends AuthGuard('jwt')
  implements CanActivate {
  public constructor(private readonly authService: AuthService) {
    super();
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    if (!(await super.canActivate(context))) {

      return false;
    }

    const request: Request = context.switchToHttp().getRequest();
    const token: string = request.headers.authorization;

    if ((request.user as ShowUserDTO).banned) {
      throw new BadRequestException('User banned');
    }

    if (!token || await this.authService.isTokenBlacklisted({ token })) {
      throw new UnauthorizedException('Jwt Access Guard');
    }

    return true;
  }
}
