import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { AuthService } from '../../auth/auth.service';
import { JWTPayload } from '../../auth/models/jwtPayload.dto';

@Injectable()
export class AuthGuardAdmin extends AuthGuard('jwt')
  implements CanActivate {
  public constructor(private readonly authService: AuthService) {
    super();
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    if (!(await super.canActivate(context))) {
      return false;
    }

    const request: Request = context.switchToHttp().getRequest();
    const token: string = request.headers.authorization;

    if ((request.user as JWTPayload).banned) {
      throw new BadRequestException('User banned');
    }

    return !(await this.authService.isTokenBlacklisted({ token })) && this.authService.isAdmin(request.user as JWTPayload);
  }
}
