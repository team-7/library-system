import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { AuthService } from '../../auth/auth.service';
import { UserOperationError } from '../exceptions/users/users.exception';

@Injectable()
export class NotLoggedGuard extends AuthGuard('jwt')
  implements CanActivate {
  public constructor(private readonly authService: AuthService) {
    super();
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: Request = context.switchToHttp().getRequest();
    const token: string | undefined = request.headers.authorization;

    if (!token || await this.authService.isTokenBlacklisted({ token })) {
        return true;
    }

    await this.authService.verifyToken(token);

    throw new UserOperationError('User already logged', 401, undefined);
  }
}
