export class GlobalException extends Error {

    private errorName: string;
    private username: string | undefined;

    constructor(msg: string, private readonly code: number, username: string | undefined) {
        super(msg);
        this.setUsername = username;
    }

    public getStatus(): number {
        return this.code;
    }

    public getErrorName(): string {
        return this.errorName;
    }

    public getUsername(): string {
        return this.username;
    }

    public set setErrorName(name: string) {
        this.errorName = name;
    }

    public set setUsername(name: string | undefined) {
        this.username = name;
    }

}
