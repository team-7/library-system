import { GlobalException } from '../global.exception';

export class UserOperationError extends GlobalException {
    constructor(msg: string, code: number, username: string) {
        super(msg, code, username);
        super.setErrorName = UserOperationError.name;
        super.setUsername = username;
    }
}
