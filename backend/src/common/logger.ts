import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as os from 'os';

@Injectable()
export class Logger {

    // path - put do papkata na logovete, name - imeto na papkata na usera, type - faila v koito da pishe(errors.txt, activity.txt)
    async write(path: string, name: string, type: string, msg: string) {
        const pathToFile = `${path}/${name}/${type}.txt`;
        const stream = fs.createWriteStream(pathToFile, { flags: 'a'});
        // fs.open(pathToFile, 'w', (openError, fd) => {

        //     if (openError) {
        //         console.log(openError);

        //     }

        const data: string = `${(new Date(Date.now())).toString()} - ${type.toUpperCase()} - ${msg}\n`;
        stream.write(data);
        stream.end();
        // });
    }

    // path to log folder
    async makeDirOnUserCreation(name: string) {
        const userFolderPath = `${os.homedir()}/Desktop/Logs/${name}`;

        this.makeDir(userFolderPath);
        this.createFile(`${userFolderPath}/error.txt`);
        this.createFile(`${userFolderPath}/activity.txt`);

    }

    private async makeDir(path: string) {
        fs.mkdir( path, error => {
            if (error) {
            }
        });
    }

    private async createFile(path: string) {
        fs.writeFile(path, '', (error) => { /* Fail silently */});
    }

}
