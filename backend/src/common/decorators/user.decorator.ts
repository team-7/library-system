import { createParamDecorator } from '@nestjs/common';
import { JWTPayload } from '../../auth/models/jwtPayload.dto';

export const User = createParamDecorator((_, req) => (req.user as JWTPayload));
