import { ArgumentsHost, Catch, ExceptionFilter, Inject } from '@nestjs/common';
import { Request, Response } from 'express';
import { GlobalException } from '../exceptions/global.exception';
import { Logger } from '../logger';

@Catch(GlobalException)
export class GlobalExceptionFilter<T extends GlobalException> implements ExceptionFilter {

  constructor(@Inject(Logger) private readonly logger: Logger) {

  }

  catch(exception: T, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request: Request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();

    if (request.headers.authorization && exception.getUsername()) {
      const msg = `${request.method} request to ${request.url} failed. Exception message: ${exception.message}\n`;
      const homedir = require('os').homedir();

      this.logger.write(`${homedir}/Desktop/Logs`, exception.getUsername(), 'error', msg);
    }

    response
      .status(status)
      .json({
        statusCode: status,
        errName: exception.getErrorName,
        message: exception.message,
      });
  }
}
