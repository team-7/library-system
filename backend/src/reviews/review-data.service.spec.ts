import { ReviewsDataService } from './review-data.service';
import { TestingModule, Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Review } from '../database/entities/review.entity';
import { Book } from '../database/entities/books.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/votes.entity';
import { Flag } from '../database/entities/flags.entity';
import { ShowBookDTO } from '../books/models/showBookDTO';
import { ShowReviewDTO } from './models/show-review.dto';
import { STATUS_CODES } from 'http';

describe('Test Review Data Service', () => {
    let service: ReviewsDataService;

    let reviewRepository: any;
    let bookRepository: any;
    let userRepository: any;
    let voteRepository: any;
    let flagsRepository: any;

    beforeEach(async () => {
        reviewRepository = {
          findOne() {
            /* empty */
          },
          create() {
            /* empty */
          },
          save() {
            /* empty */
          },
          createQueryBuilder() {
              return this;
          },
          select() {
            return this;
          },
          from() {
            return this;
          },
          where() {
            return this;
          },
          getOne() {
            /*empty*/
          },
        };

        bookRepository = {
          findOne() {
            /*empty*/
          },
          createQueryBuilder() {
            return this;
          },
          leftJoinAndSelect() {
              return this;
          },
          where() {
            return this;
          },
          getOne() {
            /*empty*/
            return 1;
          },
        };

        userRepository = {
            findOne() {
                /*empty*/
            },
        };

        voteRepository = {
            find() {
                /*empty*/
            },
        };

        flagsRepository = {
            find() {
                /*empty*/
            },
        };

        const module: TestingModule = await Test.createTestingModule({
          providers: [
            ReviewsDataService,
            { provide: getRepositoryToken(Review), useValue: reviewRepository },
            { provide: getRepositoryToken(Book), useValue: bookRepository },
            { provide: getRepositoryToken(User), useValue: userRepository },
            { provide: getRepositoryToken(Vote), useValue: voteRepository },
            { provide: getRepositoryToken(Flag), useValue: flagsRepository },
          ],
        }).compile();

        service = module.get<ReviewsDataService>(ReviewsDataService);
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('allReviews()', () => {

      it('should call reviewsRepository findOne() once if the user has Admin role', async () => {
        // Arrange
        const spyFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue(Promise.resolve({reviews: []}));

        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '1';

        // Act
        await service.allReviews(fakeAdmin, fakeBookId);

        // Assert
        expect(spyFindOne).toBeCalledTimes(1);
      });

      it('should call reviewsRepository findOne() with the correct params if the user has Admin role', async () => {
        // Arrange
        const spyFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue(Promise.resolve({reviews: []}));

        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '1';

        // Act
        await service.allReviews(fakeAdmin, fakeBookId);

        // Assert
        expect(spyFindOne).toBeCalledWith(fakeBookId, {
          relations: ['reviews'],
        });
      });

      it('should call reviewsRepository createQueryBuilder().leftJoinAndSelect().where().getOne() once if the user has no Admin role', async () => {
        // Arrange
        const spyGetOne = jest
          .spyOn(bookRepository, 'getOne')
          .mockReturnValue(Promise.resolve({reviews: []}));

        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};

        const fakeBookId = '1';

        // Act
        await service.allReviews(fakeAdmin, fakeBookId);

        // Assert
        expect(spyGetOne).toBeCalledTimes(1);
      });

      it('should throw an Error if the bookId is invalid', async () => {
        // Arrange
        const spyFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockImplementation(async () => null);

        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '1';

        // Act and Assert
        expect(service.allReviews(fakeAdmin, fakeBookId)).rejects.toThrow();
      });

      it('should return all the reviews if the user has a Admin role', async () => {
        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '1';

        const fakeReviews = [
          {
            id: '1',
            text: 'good book',
            isDeleted: false,
            date: 'date',
            voteUpCount: 2,
            voteDownCount: 2,
            flagsCount: 2,
          },
          {
            id: '2',
            text: 'worst book',
            isDeleted: true,
            date: 'date',
            voteUpCount: 1,
            voteDownCount: 1,
            flagsCount: 0,
          },
        ];

        const expectedReviews = [{
            id: '1',
            text: 'good book',
            date: 'date',
            voteUpCount: 2,
            voteDownCount: 2,
            flagsCount: 2,
          },
          {
            id: '2',
            text: 'worst book',
            date: 'date',
            voteUpCount: 2,
            voteDownCount: 2,
            flagsCount: 2,
        }];

        const spyBookRepoFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue(Promise.resolve({
            reviews: Promise.all(fakeReviews),
          }));

        const spyVoteRepoFindOne = jest
          .spyOn(voteRepository, 'find')
          .mockReturnValue(Promise.resolve([1, 2]));

        const spyFlagRepoFind = jest
        .spyOn(flagsRepository, 'find')
        .mockReturnValue(Promise.resolve([1, 2]));

        const result = await service.allReviews(fakeAdmin, fakeBookId);

        // Assert
        expect(result.length).toBe(2);
        expect(result[0]).toBeInstanceOf(ShowReviewDTO);
        expect(result).toEqual(expectedReviews);
      });

      it('should return all the reviews if the user has no Admin role', async () => {

        // Arange
        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};

        const fakeBookId = '1';

        const fakeReviews = [
          {
            id: '1',
            text: 'good book',
            isDeleted: false,
            date: 'date',
            voteUpCount: 2,
            voteDownCount: 2,
            flagsCount: 2,
          },
          {
            id: '2',
            text: 'worst book',
            isDeleted: true,
            date: 'date',
            voteUpCount: 1,
            voteDownCount: 1,
            flagsCount: 0,
          },
        ];

        const expectedReviews = [{
            id: '1',
            text: 'good book',
            date: 'date',
            voteUpCount: 2,
            voteDownCount: 2,
            flagsCount: 2,
          }];

        const spyGeOne = jest
          .spyOn(bookRepository, 'getOne')
          .mockReturnValue(Promise.resolve({
            reviews: [fakeReviews[0]],
        }));

        const spyVoteRepoFind = jest
          .spyOn(voteRepository, 'find')
          .mockReturnValue(Promise.resolve([1, 2]));

        const spyFlagRepoFind = jest
        .spyOn(flagsRepository, 'find')
        .mockReturnValue(Promise.resolve([1, 2]));

        // Act
        const result = await service.allReviews(fakeAdmin, fakeBookId);

        // Assert
        expect(result.length).toBe(1);
        expect(result[0]).toBeInstanceOf(ShowReviewDTO);
        expect(result).toEqual(expectedReviews);
      });

    });

    describe('createReview()', () => {

      it('should call bookRepository findOne() once', async () => {

        // Arange
        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '1';

        const fakeReview = {
          text: 'great book!',
        };

        const spyBookRepoFindeOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue({
            reviews: [],
          });

        const spyReviewRepoCreate = jest
          .spyOn(reviewRepository, 'create')
          .mockReturnValue(Promise.resolve({}));

        // Act
        await service.createReview(fakeAdmin, fakeBookId, fakeReview);

        expect(spyBookRepoFindeOne).toBeCalledTimes(1);
      });

      it('should call userRepository findOne() once', async () => {

        // Arange
        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '1';

        const fakeReview = {
          text: 'great book!',
        };

        const spyBookRepoCreate = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue({
            reviews: [],
        });

        const spyReviewRepoCreate = jest
          .spyOn(reviewRepository, 'create')
          .mockReturnValue(Promise.resolve({}));

        // Act
        await service.createReview(fakeAdmin, fakeBookId, fakeReview);

        expect(spyReviewRepoCreate).toBeCalledTimes(1);
      });

      it('should call userRepository findOne() once', async () => {

        // Arange
        const fakeAdmin = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '1';

        const fakeReview = {
          text: 'great book!',
        };

        const spyBookRepoFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue({
            reviews: [],
        });

        const spyReviewRepoCreate = jest
          .spyOn(reviewRepository, 'create')
          .mockReturnValue(Promise.resolve({}));

        // Act
        await service.createReview(fakeAdmin, fakeBookId, fakeReview);

        expect(spyBookRepoFindOne).toBeCalledTimes(1);
      });

      it('should call reviewRepository create() once', async () => {

        // Arange
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};

        const fakeBookId = '1';

        const fakeReview = {
          text: 'great book!',
        };

        const spyBookRepoFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue({
            reviews: [],
        });

        const spyReviewRepoCreate = jest
          .spyOn(reviewRepository, 'create')
          .mockReturnValue(Promise.resolve({}));

        // Act
        await service.createReview(fakeUser, fakeBookId, fakeReview);

        expect(spyReviewRepoCreate).toBeCalledTimes(1);
      });

      it('should throw an Error if the bookId is invalid', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '2';

        const fakeReview = {
          text: 'great book!',
        };

        const spyBookRepoFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue(undefined);

        expect(service.createReview(fakeUser, fakeBookId, fakeReview)).rejects.toThrow();
      });

      it('should create and return a new review', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeBookId = '2';

        const fakeText = {
          text: 'great book!',
        };

        const spyBookRepoFindOne = jest
          .spyOn(bookRepository, 'findOne')
          .mockReturnValue(Promise.resolve({reviews: []}));

        const spyReviewRepoCreate = jest
          .spyOn(reviewRepository, 'create')
          .mockReturnValue({id: '2', text: 'great book'});

        const expectedReview = {
          id: '2',
          text: 'great book',
        };

        const result = await service.createReview(fakeUser, fakeBookId, fakeText);

        expect(result).toEqual(expectedReview);

      });
    });

    describe('updateReview()', () => {
      it('should call findReviewById() once', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'worst book', isDeleted: false});

        await service.updateReview(fakeUser, fakeReview, fakeText);

        expect(spyReviewRepoGetOne).toBeCalledTimes(1);
      });

      it('should call reviewRepository and return the old review', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'good book', isDeleted: false});

        const expectedReturn = {
            id: '2',
            text: 'good book',
            isDeleted: false,
          };

        await service.updateReview(fakeUser, fakeReview, fakeText);

        expect(spyReviewRepoGetOne()).toEqual(expectedReturn);
      });

      it('should throw an Error if the user has no Admin rolse', () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'good book', isDeleted: false});

        expect(service.updateReview(fakeUser, fakeReview, fakeText)).rejects.toThrow();
      });

      it('should throw an Error if the user is not a owner of the review', () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({
            id: '2',
            text: 'good book',
            isDeleted: false,
            user: {
                username: 'Gosho',
              },
            });

        expect(service.updateReview(fakeUser, fakeReview, fakeText)).rejects.toThrow();
      });

      it('should update the old review and return it', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'good book', isDeleted: false});

        const expectedReturn = {
            id: '2',
            text: 'worst book',
            isDeleted: false,
          };

        const result = await service.updateReview(fakeUser, fakeReview, fakeText);

        expect(result.text).toMatch(expectedReturn.text);
      });

    });

    describe('deleteReview(', () => {
      it('should call findReviewById() once', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'worst book', isDeleted: false});

        await service.updateReview(fakeUser, fakeReview, fakeText);

        expect(spyReviewRepoGetOne).toBeCalledTimes(1);
      });

      it('should call reviewRepository and return the old review', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'good book', isDeleted: false});

        const expectedReturn = {
            id: '2',
            text: 'good book',
            isDeleted: false,
          };

        await service.updateReview(fakeUser, fakeReview, fakeText);

        expect(spyReviewRepoGetOne()).toEqual(expectedReturn);
      });

      it('should throw an Error if the user has no Admin rolse', () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'good book', isDeleted: false});

        expect(service.updateReview(fakeUser, fakeReview, fakeText)).rejects.toThrow();
      });

      it('should throw an Error if the user is not a owner of the review', () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({
            id: '2',
            text: 'good book',
            isDeleted: false,
            user: {
                username: 'Gosho',
              },
            });

        expect(service.updateReview(fakeUser, fakeReview, fakeText)).rejects.toThrow();
      });

      it('should delete the review', async () => {
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};

        const fakeReview = '2';

        const fakeText = {
          text: 'worst book!',
        };

        const spyReviewRepoGetOne = jest
          .spyOn(reviewRepository, 'getOne')
          .mockReturnValue({ id: '2', text: 'good book', isDeleted: false});

        const expectedReturn = {
            id: '2',
            text: 'worst book',
            isDeleted: true,
          };

        const spyReviewRepoSave = jest
          .spyOn(reviewRepository, 'save')
          .mockReturnValue({
            id: '2',
            text: 'worst book',
            isDeleted: true,
          });

        const result = await service.updateReview(fakeUser, fakeReview, fakeText);

        expect(spyReviewRepoSave()).toEqual(expectedReturn);
      });
    });
});
