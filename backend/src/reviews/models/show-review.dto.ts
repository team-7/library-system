import { Expose } from 'class-transformer';

export class ShowReviewDTO {

  @Expose()
  public id: string;

  @Expose()
  public text: string;

  @Expose()
  public date: Date;

  @Expose()
  public voteUpCount: number;

  @Expose()
  public voteDownCount: number;

  @Expose()
  public flagsCount: number;

  @Expose()
  public user: {
    username: string,
    roles: string[],
  };

  @Expose()
  public usersWithUpVote: string[];

  @Expose()
  public usersWithDownVote: string[];

  @Expose()
  public usersWithFlag: string[];

  @Expose()
  public isDeleted: boolean;

}
