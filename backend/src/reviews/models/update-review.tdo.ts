import { IsNotEmpty, Length } from 'class-validator';

export class UpdateReviewDTO {

  @IsNotEmpty({message: 'Review text should not be empty!'})
  @Length(5, 500, {message: 'Review text should be in the range of 5 and 100 chars!'})
  public text: string;

}
