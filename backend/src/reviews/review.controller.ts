import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Put, UseGuards, ValidationPipe } from '@nestjs/common';
import { JWTPayload } from '../auth/models/jwtPayload.dto';
import { User } from '../common/decorators';
import { AccessGuard } from '../common/guards';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { CreateReviewDTO } from './models/create-review.dto';
import { ResponseMessageDTO } from './models/response-message.dto';
import { ShowReviewDTO } from './models/show-review.dto';
import { UpdateReviewDTO } from './models/update-review.tdo';
import { ReviewsDataService } from './review-data.service';

@Controller('books/:bookId/reviews')
export class ReviewsController {
  public constructor(private readonly reviewsDataService: ReviewsDataService) {}

  @UseGuards(AccessGuard)
  @Get()
  @HttpCode(HttpStatus.OK)
  public async allReviews(
    @User() user: JWTPayload,
    @Param('bookId') bookId: string,
  ): Promise<ShowReviewDTO[]> {
    return await this.reviewsDataService.allReviews(user, bookId);
  }

  @UseGuards(AccessGuard)
  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async createReview(
    @User() user: JWTPayload,
    @Param('bookId') bookid: string,
    @Body(new ValidationPipe({whitelist: true, transform: true})) body: CreateReviewDTO,
  ): Promise<ShowReviewDTO> {
    return await this.reviewsDataService.createReview(user, bookid, body);
  }

  @UseGuards(AccessGuard)
  @Put('/:reviewId')
  @HttpCode(HttpStatus.OK)
  public async updateReview(
    @User() user: JWTPayload,
    @Param('reviewId') reviewId: string,
    @Body(new ValidationPipe({whitelist: true, transform: true})) body: UpdateReviewDTO,
  ): Promise<ShowReviewDTO> {
    return await this.reviewsDataService.updateReview(user, reviewId, body);
  }

  @UseGuards(AccessGuard)
  @Delete('/:reviewiId')
  @HttpCode(HttpStatus.OK)
  public async deleteReview(
    @User() user: JWTPayload,
    @Param('reviewiId') reviewId: string,
  ): Promise<ResponseMessageDTO> {
    await this.reviewsDataService.deleteReview(user, reviewId);

    return { message: 'The review was deleted successfully!' };
  }
}
