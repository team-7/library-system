import { Book } from './../database/entities/books.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Review } from '../database/entities/review.entity';
import { ReviewsDataService } from './review-data.service';
import { ReviewsController } from './review.controller';
import { Vote } from '../database/entities/votes.entity';
import { Flag } from '../database/entities/flags.entity';
import { User } from '../database/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Review, Book, User, Vote, Flag])],
  controllers: [ReviewsController],
  providers: [ReviewsDataService],
})
export class ReviewsModule {}
