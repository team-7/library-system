import { ReviewsController } from './review.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { ReviewsDataService } from './review-data.service';

describe('Test Review Controller', () => {
    let controller: ReviewsController;

    let reviewDataservice: any;

    beforeEach(async () => {
        reviewDataservice = {
            allReviews() {
                /*empty*/
            },
            createReview() {
                 /*empty*/
            },
            updateReview() {
                /*empty*/
           },
           deleteReview() {
                /*empty*/
            },
        };

        const module: TestingModule = await Test.createTestingModule({
            controllers: [ReviewsController],
            providers: [
              {
                provide: ReviewsDataService,
                useValue: reviewDataservice,
              },
            ],
          }).compile();

        controller = module.get<ReviewsController>(ReviewsController);

    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(controller).toBeDefined();
    });

    describe('allReviews()', () => {
        it('should call reviewDataservice allReviews() once', async () => {
          // Arrange
          const bookId = '1';
          const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
          const fakeReivews = [{ id: '1' }, { id: '2' }];

          const spyAllReviews = jest
            .spyOn(reviewDataservice, 'allReviews')
            .mockReturnValue(Promise.resolve(fakeReivews));

          // Act
          await controller.allReviews(fakeUser, bookId);

          // Assert
          expect(spyAllReviews).toBeCalledTimes(1);
        });

        it('should be called with correct params', async () => {
          // Arrange
          const bookId = '1';
          const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
          const fakeReivews = [{ id: '1' }, { id: '2' }];

          const spyAllReviews = jest
            .spyOn(reviewDataservice, 'allReviews')
            .mockReturnValue(Promise.resolve(fakeReivews));

          // Act
          await controller.allReviews(fakeUser, bookId);

          // Assert
          expect(spyAllReviews).toBeCalledWith(fakeUser, bookId);
        });

        it('should return all the correct values', async () => {
          // Arrange
          const bookId = '1';
          const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};
          const fakeReivews = [{ id: '1',  isDeleted: false }, { id: '2',  isDeleted: true }];

          jest
            .spyOn(reviewDataservice, 'allReviews')
            .mockReturnValue(Promise.resolve(fakeReivews));

          // Act
          const result = await controller.allReviews(fakeUser, bookId);

          // Assert
          expect(result).toEqual(fakeReivews);
        });

    });

    describe('createReview()', () => {
      it('should call reviewDataservice createReview() once', async () => {
        // Arrange
        const bookId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
        const fakeReivews = [{ id: '1' , text: 'good book'}];

        const spyCreateReview = jest
          .spyOn(reviewDataservice, 'createReview')
          .mockReturnValue(Promise.resolve(fakeReivews));

        // Act
        await controller.createReview(fakeUser, bookId, {text: 'worst book!'});

        // Assert
        expect(spyCreateReview).toBeCalledTimes(1);
      });

      it('should be called with correct params', async () => {
        // Arrange
        const bookId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
        const fakeReivews = [{ id: '1' , text: 'good book'}];

        const spyCreateReview = jest
          .spyOn(reviewDataservice, 'createReview')
          .mockReturnValue(Promise.resolve(fakeReivews));

        // Act
        await controller.createReview(fakeUser, bookId, {text: 'worst book!'});

        // Assert
        expect(spyCreateReview).toBeCalledWith(fakeUser, bookId, {text: 'worst book!'});
      });

      it('should return a new review', async () => {
        // Arrange
        const bookId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};
        const fakeReivews = [{ id: '1' , text: 'worst book!'}];

        jest
          .spyOn(reviewDataservice, 'createReview')
          .mockReturnValue(Promise.resolve(fakeReivews));

        // Act
        const result = await controller.createReview(fakeUser, bookId, {text: 'worst book!'});

        // Assert
        expect(result).toEqual(fakeReivews);
      });
    });

    describe('updateReview()', () => {
      it('should call reviewDataservice updateReview() once', async () => {
        // Arrange
        const reviewId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
        const fakeReivews = [{ id: '1' , text: 'good book'}];

        const spyUpdateReview = jest
          .spyOn(reviewDataservice, 'updateReview')
          .mockReturnValue(Promise.resolve(fakeReivews));

        // Act
        await controller.updateReview(fakeUser, reviewId, {text: 'worst book!'});

        // Assert
        expect(spyUpdateReview).toBeCalledTimes(1);
      });

      it('should be called with correct params', async () => {
        // Arrange
        const bookId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
        const fakeReivew = [{ id: '1' , text: 'good book'}];

        const spyUpdateReview = jest
          .spyOn(reviewDataservice, 'updateReview')
          .mockReturnValue(Promise.resolve(fakeReivew));

        // Act
        await controller.updateReview(fakeUser, bookId, {text: 'worst book!'});

        // Assert
        expect(spyUpdateReview).toBeCalledWith(fakeUser, bookId, {text: 'worst book!'});
      });

      it('should update the old review and return it', async () => {
        // Arrange
        const bookId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
        const fakeReivews = [{ id: '1' , text: 'worst book!'}, { id: '2', text: 'I liked it'}];

        jest
          .spyOn(reviewDataservice, 'updateReview')
          .mockReturnValue(Promise.resolve(fakeReivews));

        // Act
        const result = await controller.updateReview(fakeUser, bookId, {text: 'worst book!'});

        // Assert
        expect(result).toEqual(fakeReivews);
      });
    });

    describe('deleteReview()', () => {
      it('should call reviewDataservice deleteReview() once', async () => {
        // Arrange
        const reviewId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
        const fakeMessage = {message: 'Review was deleted'};

        const spyDeleteReview = jest
          .spyOn(reviewDataservice, 'deleteReview')
          .mockReturnValue(Promise.resolve(fakeMessage));

        // Act
        await controller.deleteReview(fakeUser, reviewId);

        // Assert
        expect(spyDeleteReview).toBeCalledTimes(1);
      });

      it('should be called with correct params', async () => {
        // Arrange
        const bookId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['Admin'], banned: false};
        const fakeMessage = {message: 'Review was deleted'};

        const spyDeleteReview = jest
          .spyOn(reviewDataservice, 'deleteReview')
          .mockReturnValue(Promise.resolve(fakeMessage));

        // Act
        await controller.deleteReview(fakeUser, bookId);

        // Assert
        expect(spyDeleteReview).toBeCalledWith(fakeUser, bookId);
      });

      it('should return a success message if the user successfully delete the review', async () => {
        // Arrange
        const fakeReviewId = '1';
        const fakeUser = {id: '2', username: 'Anton', email: '1234', roles: ['User'], banned: false};
        const fakeMessage = 'Review Deleted!';

        jest
          .spyOn(reviewDataservice, 'deleteReview')
          .mockReturnValue(Promise.resolve(fakeMessage));

        // Act
        const result = await controller.deleteReview(fakeUser, fakeReviewId);

        // Assert
        expect(result.message).toMatch(fakeMessage);
      });
    });

});
