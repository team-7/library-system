import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { JWTPayload } from '../auth/models/jwtPayload.dto';
import { GlobalException } from '../common/exceptions/global.exception';
import { Flag } from '../database/entities/flags.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/votes.entity';
import { VoteTypes } from '../votes/enums/vote-type.enum';
import { Book } from './../database/entities/books.entity';
import { Review } from './../database/entities/review.entity';
import { CreateReviewDTO } from './models/create-review.dto';
import { ShowReviewDTO } from './models/show-review.dto';
import { UpdateReviewDTO } from './models/update-review.tdo';

@Injectable()
export class ReviewsDataService {
  public constructor(
    @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Vote) private readonly voteRepository: Repository<Vote>,
    @InjectRepository(Flag) private readonly flagsRepository: Repository<Flag>,
  ) {}

  public async allReviews(
    user: JWTPayload,
    bookId: string,
  ): Promise<ShowReviewDTO[]> {
    let foundBook: Book;
    if (user.roles.includes('Admin')) {
      foundBook = await this.bookRepository.findOne(bookId, {
        relations: ['reviews'],
      });
    } else {
      foundBook = await this.bookRepository.createQueryBuilder('books')
        .leftJoinAndSelect('books.reviews', 'review', 'review.isDeleted = :isDeleted', { isDeleted: false})
        .where('books.id = :id', { id: bookId})
        .getOne();
    }
    if (!foundBook) {
      throw new GlobalException('No such book!', 404, user.username);
    }

    const reviews: Review[] = await foundBook.reviews;
    const mappedReview = reviews.map(async o => {
      const info = await this.getReviewInfo(o.id);
      return {
        id: o.id,
        text: o.text,
        date: o.date,
        isDeleted: o.isDeleted,
        voteUpCount: info.ups,
        voteDownCount: info.downs,
        flagsCount: info.flags,
        usersWithUpVote: [...info.usersWithUpVote],
        usersWithDownVote: [...info.usersWithDownVote],
        usersWithFlag: [...info.usersWithFlag],
        user: {
          username: info.user.username,
          roles: [...info.user.roles],
        },
      };
    });
    const plainReviews = await Promise.all(mappedReview);

    const sortedPlainReviews = plainReviews.sort(
      (a, b) => {
        return b.date.getTime() - a.date.getTime();
      },
    );

    return plainToClass(ShowReviewDTO, sortedPlainReviews, {
      excludeExtraneousValues: true,
    });
  }

  public async createReview(
    user: JWTPayload,
    bookId: string,
    review: CreateReviewDTO,
  ): Promise<ShowReviewDTO> {
    const foundBook: Book = await this.bookRepository.findOne(bookId);

    if (!foundBook) {
      throw new GlobalException('No such book!', 404, user.username);
    }

    const foundUser: User = await this.userRepository.findOne({
      username: user.username,
    });

    const reviewEntity: Review = this.reviewRepository.create(review);

    reviewEntity.book = Promise.resolve(foundBook);
    reviewEntity.user = Promise.resolve(foundUser);
    await this.reviewRepository.save(reviewEntity);

    return plainToClass(ShowReviewDTO, this.entityToPlain(reviewEntity, user), {
      excludeExtraneousValues: true,
    });
  }

  public async updateReview(
    user: JWTPayload,
    reviewId: string,
    updatedReview: UpdateReviewDTO,
  ): Promise<ShowReviewDTO> {
    const oldReview: Review = await this.findReviewById(reviewId);

    if (user.roles.includes('Admin') || (await oldReview.user).username === user.username) {
      const entityToUpdate: Review = { ...oldReview, ...updatedReview };
      await this.reviewRepository.save(entityToUpdate);

      return plainToClass(ShowReviewDTO, this.entityToPlain(entityToUpdate, user), {
        excludeExtraneousValues: true,
      });
    } else {
      throw new GlobalException('You cannot update this review!', 401, user.username);
    }

  }

  public async deleteReview(
    user: JWTPayload,
    reviewId: string,
    ): Promise<ShowReviewDTO> {
    const reviewToDelete: Review = await this.findReviewById(reviewId);
    if (user.roles.includes('Admin') || (await reviewToDelete.user).username === user.username) {
        reviewToDelete.isDeleted = true;

        await this.reviewRepository.save(reviewToDelete);

        return plainToClass(ShowReviewDTO, this.entityToPlain(reviewToDelete, user), {
        excludeExtraneousValues: true,
      });
    } else {
      throw new GlobalException('You cannot delete this review!', 401, user.username);
    }
  }

  private async findReviewById(reviewId: string): Promise<Review> {
    const foundReview = await this.reviewRepository
      .createQueryBuilder()
      .select('reviews')
      .from(Review, 'reviews')
      .where('reviews.id = :id', { id: reviewId })
      .getOne();
    if (foundReview === undefined || foundReview.isDeleted) {
      throw new GlobalException('No such review!', 404, undefined);
    }

    return foundReview;
  }

  private async getReviewInfo(reviewId: string) {

    const ups = await this.voteRepository.find({
      where: {
        review: reviewId,
        voteType: VoteTypes.Up,
        isDeleted: false,
      },
      relations: ['user'],
    });

    const upVotesUsers = await Promise.all(ups.map(async o => await o.user.then(u => u.username)));

    const downs = await this.voteRepository.find({
      where: {
        review: reviewId,
        voteType: VoteTypes.Down,
        isDeleted: false,
      },
      relations: ['user'],
    });

    const downVotesUsers = await Promise.all(downs.map(async o => await o.user.then(u => u.username)));

    const flags = await this.flagsRepository.find({
      where: {
        review: reviewId,
        isDeleted: false,
      },
      relations: ['user'],
    });

    const flagsUsers = await Promise.all(flags.map(async o => await o.user.then(u => u.username)));

    const review: Review = await this.reviewRepository.findOne(reviewId, {
      relations: ['user'],
    });
    const user: User = await review.user;
    const userRoles = user.roles.map(o => o.name);

    return {
      ups: ups.length,
      downs: downs.length,
      flags: flags.length,
      usersWithUpVote: upVotesUsers,
      usersWithDownVote: downVotesUsers,
      usersWithFlag: flagsUsers,
      user: {
        username: user.username,
        roles: [...userRoles],
      },
    };
   }

  private entityToPlain(reviewEntity: Review, user: JWTPayload): ShowReviewDTO {
    return {
      id: reviewEntity.id,
      text: reviewEntity.text,
      date: reviewEntity.date,
      voteDownCount: 0,
      voteUpCount: 0,
      isDeleted: reviewEntity.isDeleted,
      flagsCount: 0,
      usersWithUpVote: [],
      usersWithDownVote: [],
      usersWithFlag: [],

      user: {
        username: user.username,
        roles: [...user.roles],
      },
    };
  }
}
