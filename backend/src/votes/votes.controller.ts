import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, UseGuards, ValidationPipe, Put } from '@nestjs/common';
import { User } from '../common/decorators';
import { AccessGuard } from '../common/guards';
import { ResponseMessageDTO } from '../reviews/models/response-message.dto';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { CreateVoteDTO } from './models/create-vote.dto';
import { ShowVoteDTO } from './models/show-vote.dto';
import { UpdateVoteDTO } from './models/update-vote.dto';
import { VoteDataService } from './votes.service';
import { JWTPayload } from '../auth/models';

@Controller('books/:bookId/reviews/:reviewId/votes')
export class VoteController {

    public constructor(
        private readonly voteDataService: VoteDataService,
    ) {}

    @UseGuards(AccessGuard)
    @Get()
    @HttpCode(HttpStatus.OK)
    public async getReviewVotes(
        @User() user: JWTPayload,
        @Param('reviewId') reviewId: string,
    ): Promise<ShowVoteDTO[]> {
        return await this.voteDataService.getReviewVotes(user, reviewId);
    }

    @UseGuards(AccessGuard)
    @Get('user')
    @HttpCode(HttpStatus.OK)
    public async getVoteIdByUser(
        @User() user: JWTPayload,
        @Param('reviewId') reviewId: string,
    ): Promise<{voteId: string}> {
        return await this.voteDataService.getVoteIdByUser(user, reviewId);
    }

    @UseGuards(AccessGuard)
    @Post()
    @HttpCode(HttpStatus.CREATED)
    public async createVote(
        @User() user: JWTPayload,
        @Body(new ValidationPipe({whitelist: true, transform: true})) newVote: CreateVoteDTO,
        @Param('reviewId') reviewId: string,
    ): Promise<ShowVoteDTO> {
        return await this.voteDataService.createVote(user, reviewId, newVote);
    }

    @UseGuards(AccessGuard)
    @Put(':voteId')
    @HttpCode(HttpStatus.OK)
    public async updateVote(
        @User() user: JWTPayload,
        @Body(new ValidationPipe({whitelist: true, transform: true})) updateVote: UpdateVoteDTO,
        @Param('reviewId') reviewId: string,
        @Param('voteId') voteId: string,
    ): Promise<ShowVoteDTO> {
        return await this.voteDataService.updateVote(user, reviewId, voteId, updateVote);
    }

    @UseGuards(AccessGuard)
    @Delete(':voteId')
    @HttpCode(HttpStatus.OK)
    public async deleteVote(
        @User() user: JWTPayload,
        @Param('reviewId') reviewId: string,
        @Param('voteId') voteId: string,
    ): Promise<ResponseMessageDTO> {
        await this.voteDataService.deleteVote(user, reviewId, voteId);

        return {message: 'The vote was deleted'};
    }
}
