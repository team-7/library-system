import { VoteTypes } from './../enums/vote-type.enum';
import { Expose } from 'class-transformer';
import { ShowReviewDTO } from '../../reviews/models/show-review.dto';

export class ShowVoteDTO {

    @Expose()
    public id: string;

    @Expose()
    public voteType: VoteTypes;

}
