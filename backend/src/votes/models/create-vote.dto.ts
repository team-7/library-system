import { VoteTypes } from './../enums/vote-type.enum';
import { IsEnum } from 'class-validator';

export class CreateVoteDTO {
    @IsEnum(VoteTypes, { message: 'The type of the vote should be on of the following: Like, Heart, Angry, Sad, Haha, Wow'})
    public voteType: VoteTypes;
}
