import { VoteTypes } from './../enums/vote-type.enum';
import { IsEnum } from 'class-validator';

export class UpdateVoteDTO {
    @IsEnum(VoteTypes, { message: 'The type vote the vote should be on of the following'})
    public voteType: VoteTypes;
}
