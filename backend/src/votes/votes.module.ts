import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VoteController } from './votes.controller';
import { VoteDataService } from './votes.service';
import { Review } from './../database/entities/review.entity';
import { Vote } from './../database/entities/votes.entity';
import { User } from '../database/entities/user.entity';
@Module({
    imports: [TypeOrmModule.forFeature([Review, Vote, User])],
    controllers: [VoteController],
    providers: [VoteDataService],
})

export class VoteModule {}
