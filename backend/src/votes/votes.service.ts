import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { JWTPayload } from '../auth/models/jwtPayload.dto';
import { GlobalException } from '../common/exceptions/global.exception';
import { User } from '../database/entities/user.entity';
import { Review } from './../database/entities/review.entity';
import { Vote } from './../database/entities/votes.entity';
import { CreateVoteDTO } from './models/create-vote.dto';
import { ShowVoteDTO } from './models/show-vote.dto';
import { UpdateVoteDTO } from './models/update-vote.dto';

@Injectable()
export class VoteDataService {

    public constructor(

        @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
        @InjectRepository(Vote) private readonly voteRepository: Repository<Vote>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
    ) {}

    public async getReviewVotes(
        user: JWTPayload,
        reviewId: string,
        ): Promise<ShowVoteDTO[]> {

        let foundReview: Review;
        if (user.roles.includes('Admin')) {
            foundReview = await this.reviewRepository.findOne(reviewId, {
            relations: ['votes'],
            });
        } else {
            foundReview = await this.reviewRepository.createQueryBuilder('reviews')
            .leftJoinAndSelect('reviews.votes', 'vote', 'vote.isDeleted = :isDeleted', { isDeleted: false})
            .where('reviews.id = :id', { id: reviewId})
            .getOne();
        }

        if (!foundReview) {
            throw new GlobalException('No such review!', 404, undefined);
        }

        const plainVotes = (await foundReview.votes).map(o => ({...o}));

        return plainToClass(ShowVoteDTO, plainVotes , {
            excludeExtraneousValues: true,
        });
    }

    public async getVoteIdByUser(
        user: JWTPayload,
        reviewId: string,
    ): Promise<{voteId: string}> {
        const voteByUser = await this.voteRepository.findOne({
            where: {reviewId, userId: user.id, isDeleted: false},
        });
        return {voteId: voteByUser.id};
    }

    public async createVote(
        user: JWTPayload,
        reviewId: string,
        newVote: CreateVoteDTO,
        ): Promise<ShowVoteDTO> {
        const foundReview: Review = await this.reviewRepository.findOne({
            id: reviewId,
        });

        if (!foundReview) {
            throw new GlobalException(`The review doesn't exist`, 404, undefined);
        }

        const foundUser: User = await this.userRepository.findOne({
            username: user.username,
        });

        const createdVote: Vote = this.voteRepository.create();
        createdVote.user = Promise.resolve(foundUser);
        createdVote.userId = foundUser.id;
        createdVote.voteType = newVote.voteType;
        createdVote.review = Promise.resolve(foundReview);

        await this.voteRepository.save(createdVote);

        return plainToClass(ShowVoteDTO, {...createdVote}, {
            excludeExtraneousValues: true,
        });
    }

    public async updateVote(
        user: JWTPayload,
        reviewId: string,
        voteId: string,
        updateVote: UpdateVoteDTO,
        ): Promise<ShowVoteDTO> {
        const foundReview: Review = await this.reviewRepository.findOne({
            id: reviewId,
        });

        if (!foundReview) {
            throw new GlobalException(`The review doesn't exist`, 404, undefined);
        }

        const voteToUpdate: Vote = await this.voteRepository.findOne({
            id: voteId,
        });

        if (!voteToUpdate) {
            throw new GlobalException(`The vote doesn't exist`, 404, undefined);
        }

        if (user.roles.includes('Admin') || (await voteToUpdate.user).username === user.username) {
            voteToUpdate.voteType = updateVote.voteType;
            await this.voteRepository.save(voteToUpdate);

            return plainToClass(ShowVoteDTO, {...voteToUpdate}, {
                excludeExtraneousValues: true,
            });
        } else {
            throw new GlobalException('You cannot delete this review!', 401, user.username);
        }

    }

    public async deleteVote(
        user: JWTPayload,
        reviewId: string,
        voteId: string,
        ): Promise<ShowVoteDTO> {
        const foundReview: Review = await this.reviewRepository.findOne({
            id: reviewId,
        });

        if (!foundReview) {
            throw new GlobalException(`The review doesn't exist`, 404, undefined);
        }

        const voteToDelete: Vote = await this.voteRepository.findOne({
            id: voteId,
        });

        if (!voteToDelete) {
            throw new GlobalException(`The vote doesn't exist`, 404, undefined);
        }

        if (user.roles.includes('Admin') || (await voteToDelete.user).username === user.username) {
            voteToDelete.isDeleted = true;
            await this.voteRepository.save(voteToDelete);
            return plainToClass(ShowVoteDTO, {...voteToDelete}, {
                excludeExtraneousValues: true,
            });
        } else {
            throw new GlobalException('You cannot delete this review!', 401, user.username);
        }

    }

}
