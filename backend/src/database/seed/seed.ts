import * as bcrypt from 'bcrypt';
import { Connection, createConnection } from 'typeorm';
import { Roles } from '../../users/enum/user-roles.enum';
import { Book } from '../entities/books.entity';
import { Review } from '../entities/review.entity';
import { Role } from '../entities/role.entity';
import { User } from '../entities/user.entity';

const main = async () => {
  const connection: Connection = await createConnection({
    type: 'mysql',
    host: 'localhost',
    port: 3307,
    username: 'root',
    password: 'root1234',
    database: 'mydb',
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
});
  const roleRepo = connection.getRepository(Role);
  const userRepo = connection.getRepository(User);
  const bookRepo = connection.getRepository(Book);
  const reviewRepo = connection.getRepository(Review);

  const adminRole: Role = await roleRepo.save({
    name: Roles.Admin,
  });

  const userRole: Role = await roleRepo.save({
    name: Roles.User,
  });

  let firstAdmin = new User();
  firstAdmin.username = 'BatBoiko';
  firstAdmin.password = await bcrypt.hash('a12345678!', 10);
  firstAdmin.email = 'batboiko@gmail.com';
  firstAdmin.roles = [adminRole, userRole];
  firstAdmin.avatar = 'assets/profile-avatars/boy.png';

  firstAdmin = await userRepo.save(firstAdmin);

  let firstUser = new User();
  firstUser.username = 'Anton';
  firstUser.password = await bcrypt.hash('a12345678!', 10);
  firstUser.email = 'anton@anton.com';
  firstUser.roles = [userRole];
  firstUser.avatar = 'assets/profile-avatars/boy.png';

  firstUser = await userRepo.save(firstUser);

  let testBook = new Book();
  testBook.name = 'Book Cover Design Formula';
  testBook.author = 'Anita Nipane';
  // tslint:disable-next-line: max-line-length
  testBook.text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam enim diam, dignissim ut massa sit amet, ornare blandit ipsum. Etiam dapibus eros sit amet efficitur mattis. Sed feugiat consectetur finibus. Phasellus eget turpis leo. Mauris vehicula, justo nec ornare interdum, tellus erat ornare lectus, quis venenatis est mi sed tellus. Phasellus nec feugiat nisi. Phasellus vitae congue tortor. Nunc accumsan accumsan dui, eget placerat odio egestas sit amet.`;
  testBook.bookImgUrl = '51I1qxcnkkL.jpg';
  testBook.user = Promise.resolve(firstAdmin);

  testBook = await bookRepo.save(testBook);

  let testBook2 = new Book();
  testBook2.name = 'Very Nice';
  testBook2.author = 'Mery Dermansky';
  // tslint:disable-next-line: max-line-length
  testBook2.text = `It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. `;
  testBook2.bookImgUrl = 'book.jpg';
  testBook2.user = Promise.resolve(firstAdmin);

  testBook2 = await bookRepo.save(testBook2);

  let testBook3 = new Book();
  testBook3.name = 'Canadian Patent Law';
  testBook3.author = 'Stephen J. Perry';
  // tslint:disable-next-line: max-line-length
  testBook3.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook3.bookImgUrl = 'canadian-patent-law-storeimage.jpg';
  testBook3.user = Promise.resolve(firstAdmin);

  testBook3 = await bookRepo.save(testBook3);

  let testBook4 = new Book();
  testBook4.name = 'Title of the Book';
  testBook4.author = 'Author Name';
  // tslint:disable-next-line: max-line-length
  testBook4.text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam enim diam, dignissim ut massa sit amet, ornare blandit ipsum. Etiam dapibus eros sit amet efficitur mattis. Sed feugiat consectetur finibus. Phasellus eget turpis leo. Mauris vehicula, justo nec ornare interdum, tellus erat ornare lectus, quis venenatis est mi sed tellus. Phasellus nec feugiat nisi. Phasellus vitae congue tortor. Nunc accumsan accumsan dui, eget placerat odio egestas sit amet.`;
  testBook4.bookImgUrl = 'creepy-little-girl.jpg';
  testBook4.user = Promise.resolve(firstAdmin);
  testBook4 = await bookRepo.save(testBook4);

  let testBook5 = new Book();
  testBook5.name = 'Shield of Hades';
  testBook5.author = 'John Premade';
  // tslint:disable-next-line: max-line-length
  testBook5.text = `It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. `;
  testBook5.bookImgUrl = 'Historical-Fantasy-Victorian-Mystery-Premade-Book-Cover-Shield-of-Hades.jpg';
  testBook5.user = Promise.resolve(firstAdmin);

  testBook5 = await bookRepo.save(testBook5);

  let testBook7 = new Book();
  testBook7.name = `You can run but can't hide`;
  testBook7.author = 'Onzi ot 300';
  // tslint:disable-next-line: max-line-length
  testBook7.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook7.bookImgUrl = 'images (1).jpg';
  testBook7.user = Promise.resolve(firstAdmin);

  testBook7 = await bookRepo.save(testBook7);

  let testBook8 = new Book();
  testBook8.name = `The little Mermaid`;
  testBook8.author = 'Hans';
  // tslint:disable-next-line: max-line-length
  testBook8.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook8.bookImgUrl = 'images (2).jpg';
  testBook8.user = Promise.resolve(firstAdmin);

  testBook8 = await bookRepo.save(testBook8);

  let testBook9 = new Book();
  testBook9.name = `Harry Potter`;
  testBook9.author = 'J. K. Rolling';
  // tslint:disable-next-line: max-line-length
  testBook9.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook9.bookImgUrl = 'images.jpg';
  testBook9.user = Promise.resolve(firstAdmin);

  testBook9 = await bookRepo.save(testBook9);

  let testBook10 = new Book();
  testBook10.name = `Killing Commend`;
  testBook10.author = 'Haruki Murakami';
  // tslint:disable-next-line: max-line-length
  testBook10.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook10.bookImgUrl = 'killing-commendatore.w460.h690.jpg';
  testBook10.user = Promise.resolve(firstAdmin);

  testBook10 = await bookRepo.save(testBook10);

  let testBook11 = new Book();
  testBook11.name = `Haunted Forest`;
  testBook11.author = 'Albert Lee';
  // tslint:disable-next-line: max-line-length
  testBook11.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook11.bookImgUrl = 'large_thumb_stage.jpg';
  testBook11.user = Promise.resolve(firstAdmin);

  testBook11 = await bookRepo.save(testBook11);

  let testBook12 = new Book();
  testBook12.name = `Torn`;
  testBook12.author = 'Paul Clarke';
  // tslint:disable-next-line: max-line-length
  testBook12.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook12.bookImgUrl = 'sad-romance-kindle-book-cover-flyer-template-539b5fdf2dd2f6602c25ce81fbb5d877_screen.jpg';
  testBook12.user = Promise.resolve(firstAdmin);

  testBook12 = await bookRepo.save(testBook12);

  let testBook13 = new Book();
  testBook13.name = `The great Gatsb`;
  testBook13.author = 'Scot Fitzerald';
  // tslint:disable-next-line: max-line-length
  testBook13.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook13.bookImgUrl = 'the-50-coolest-book-covers-19-1556672508-3Cqq-column-width-inline.jpg';
  testBook13.user = Promise.resolve(firstAdmin);

  testBook13 = await bookRepo.save(testBook13);

  let testBook14 = new Book();
  testBook14.name = `The arsonist`;
  testBook14.author = 'Chloe Hooper';
  // tslint:disable-next-line: max-line-length
  testBook14.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook14.bookImgUrl = 'the-arsonist.jpg';
  testBook14.user = Promise.resolve(firstAdmin);

  testBook14 = await bookRepo.save(testBook14);

  let testBook15 = new Book();
  testBook15.name = `The Third Plate`;
  testBook15.author = 'Onzi ot 300';
  // tslint:disable-next-line: max-line-length
  testBook15.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook15.bookImgUrl = 'thethirdplate.jpg';
  testBook15.user = Promise.resolve(firstAdmin);

  testBook15 = await bookRepo.save(testBook15);

  let testBook16 = new Book();
  testBook16.name = `The way in the forest`;
  testBook16.author = 'Onzi ot 300';
  // tslint:disable-next-line: max-line-length
  testBook16.text = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. `;
  testBook16.bookImgUrl = 'the-way-in-the-forest.jpg';
  testBook16.user = Promise.resolve(firstAdmin);

  testBook16 = await bookRepo.save(testBook16);

  let testBookReview = new Review();
  testBookReview.text = 'TestBookReviewText';
  testBookReview.user = Promise.resolve(firstUser);
  testBookReview.book = Promise.resolve(testBook);

  testBookReview = await reviewRepo.save(testBookReview);

  let testBookReview2 = new Review();
  testBookReview2.text = 'Tuka veche ne e mrasen tes da go e';
  testBookReview2.user = Promise.resolve(firstUser);
  testBookReview2.book = Promise.resolve(testBook);

  testBookReview2 = await reviewRepo.save(testBookReview2);

  let testBookReview3 = new Review();
  testBookReview3.text = 'Baaaaaaaasi qkata kniga manqk';
  testBookReview3.user = Promise.resolve(firstAdmin);
  testBookReview3.book = Promise.resolve(testBook);

  testBookReview3 = await reviewRepo.save(testBookReview3);

  await connection.close();

  // tslint:disable-next-line: no-console
  console.log(`Data seeded successfully`);
};

// tslint:disable-next-line: no-console
main().catch(console.log);
