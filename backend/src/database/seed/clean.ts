import { createConnection } from 'typeorm';
import { Role } from '../entities/role.entity';
import { User } from '../entities/user.entity';

const main = async () => {
  const connection = await createConnection();
  const roleRepository = connection.getRepository(Role);
  const userRepository = connection.getRepository(User);

  await roleRepository.delete({});
  await userRepository.delete({});

  await connection.close();

  console.log(`Data has been cleared successfully`);
};

main().catch(console.log);
