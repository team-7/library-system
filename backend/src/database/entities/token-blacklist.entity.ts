import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('blacklist')
export class TokenBlacklist {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ nullable: false, length: 1000 })
    token: string;
}
