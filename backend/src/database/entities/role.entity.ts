import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from 'typeorm';
import { Roles } from '../../users/enum/user-roles.enum';
import { User } from './user.entity';

@Entity('roles')
export class Role {
  @PrimaryGeneratedColumn('increment')
  public id: string;

  @Column({ nullable: false })
  public name: string;
}
