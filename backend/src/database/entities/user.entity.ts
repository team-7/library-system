import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Book } from './books.entity';
import { Flag } from './flags.entity';
import { Rate } from './rate.entity';
import { Review } from './review.entity';
import { Role } from './role.entity';
import { Vote } from './votes.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 20, nullable: false, unique: true })
  username: string;

  @Column({ length: 1000, nullable: false })
  password: string;

  @Column({ length: 50, nullable: false, unique: true })
  email: string;

  @Column()
  avatar: string;

  @Column({ default: false })
  isDeleted: boolean;

  @CreateDateColumn()
  date: Date;

  @Column({ default: 10 })
  gamingp: number;

  @Column({ default: 0 })
  karmap: number;

  @Column({ default: false })
  banned: boolean;

  @OneToMany(type => Review, reviews => reviews.user)
  reviews: Promise<Review[]>;

  @OneToMany(type => Book, books => books.user)
  books: Promise<Book[]>;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(type => Vote, votes => votes.user)
  votes: Promise<Vote[]>;

  @OneToMany(type => Flag, flags => flags.user)
  flags: Promise<Flag[]>;

  @OneToMany(type => Rate, rates => rates.user)
  rates: Promise<Rate[]>;

}
