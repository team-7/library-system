import { RateType } from './../../rate/models/rateTypes';
import { Book } from './books.entity';
import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Rate {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'enum', enum: RateType, nullable: false })
    public rateType: RateType;

    @Column('boolean', { nullable: false, default: false })
    public isDeleted: boolean;

    @ManyToOne(type => Book, books => books.rate)
    public book: Promise<Book>;

    @ManyToOne(type => User, users => users.rates)
    public user: Promise<User>;
}
