import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';
import { Review } from './review.entity';
import { VoteTypes } from '../../votes/enums/vote-type.enum';
import { User } from './user.entity';

@Entity('votes')
export class Vote {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ nullable: false, default: false })
    public isDeleted: boolean;

    @Column({ type: 'enum', enum: VoteTypes, nullable: false })
    public voteType: VoteTypes;

    @Column({ type: 'nvarchar', nullable: false})
    public userId: string;

    @ManyToOne(type => Review, review => review.votes)
    public review: Promise<Review>;

    @ManyToOne(type => User, user => user.votes)
    public user: Promise<User>;

}
