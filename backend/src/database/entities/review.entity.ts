import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Book } from './books.entity';
import { Vote } from './votes.entity';
import { Flag } from './flags.entity';

@Entity('reviews')
export class Review {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ nullable: false, type: 'text' })
  public text: string;

  @CreateDateColumn({ nullable: false, type: 'timestamp' })
  public date: Date;

  @ManyToOne(type => User, user => user.reviews)
  public user: Promise<User>;

  @ManyToOne(type => Book, book => book.reviews)
  public book: Promise<Book>;

  @OneToMany(type => Vote, vote => vote.review)
  public votes: Promise<Vote[]>;

  @OneToMany(type => Flag, flag => flag.review)
  public flags: Promise<Flag[]>;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @Column({ nullable: false, type: 'integer', default: 0})
  public voteUpCount: number;

  @Column({ nullable: false, type: 'integer', default: 0})
  public voteDownCount: number;

  @Column({ nullable: false, type: 'integer', default: 0})
  public flagsCount: number;
}
