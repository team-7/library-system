import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Rate } from './rate.entity';
import { Review } from './review.entity';
import { User } from './user.entity';

@Entity('books')
export class Book {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column('nvarchar', { nullable: false, unique: true })
  public name: string;

  @Column('nvarchar', { nullable: false })
  public author: string;

  @Column('text')
  public text: string;

  @Column({ type: 'boolean', default: false, nullable: false })
  public borrowed: boolean;

  @Column({ type: 'boolean', default: false, nullable: false })
  public unlisted: boolean;

  @CreateDateColumn({ nullable: false, type: 'timestamp' })
  public date: Date;

  @Column({ type: 'boolean', default: false, nullable: false })
  public isDeleted: boolean;

  @Column('nvarchar', { default: null })
  public bookImgUrl: string;

  @OneToMany(type => Review, review => review.book)
  public reviews: Promise<Review[]>;

  @ManyToOne(type => User, user => user.books)
  public user: Promise<User>;

  @OneToMany(type => Rate, rate => rate.book)
  public rate: Promise<Rate[]>;

  @Column('integer', { nullable: false , default: 0 })
    public bookRating: number;
}
