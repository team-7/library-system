import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Review } from './review.entity';
import { FlagTypes } from '../../flags/enums/flag-type.enum';
import { User } from './user.entity';

@Entity('flags')
export class Flag {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @ManyToOne(type => User, user => user.flags)
    public user: Promise<User>;

    @ManyToOne(type => Review, review => review.votes)
    public review: Promise<Review>;

    @Column({ type: 'enum', enum: FlagTypes, nullable: false })
    public flagType: FlagTypes;

    @Column({ nullable: false, default: false })
    public isChecked: boolean;
}
