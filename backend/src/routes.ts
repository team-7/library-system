import { Routes } from 'nest-router';
import { AuthModule } from './auth/auth.module';
import { BooksModule } from './books/books.module';
import { RateModule } from './rate/rate.module';
import { ReviewsModule } from './reviews/review.modul';
import { UsersModule } from './users/users.module';
import { VoteModule } from './votes/votes.module';
import { FlagsModule } from './flags/flags.module';
export const routes: Routes = [
  {
    path: '/api',
    children: [
        {
            path: '/',
            module: AuthModule,
        },
        {
            path: '/',
            module: UsersModule,
        },
        {
            path: '/',
            module: BooksModule,
        },
        {   path: '/', module:
            ReviewsModule,
        },
        {
            path: '/', module:
            RateModule,
        },
        {
            path: '/',
            module: VoteModule,
        },
        {
            path: '/',
            module: FlagsModule,
        },
    ],
  },
];
