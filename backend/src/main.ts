import { NestFactory, NestApplication } from '@nestjs/core';
import { AppModule } from './app.module';
import { GlobalExceptionFilter } from './common/filters/global.filter';
import { Logger } from './common/logger';
import { ConfigService } from './config/config.service';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'bookCovers'), {prefix: '/bookCovers'})
  app.enableCors();

  const logger = app.get<Logger>(Logger);
  app.useGlobalFilters(new GlobalExceptionFilter(logger));

  await app.listen(app.get(ConfigService).port);
}
bootstrap();
